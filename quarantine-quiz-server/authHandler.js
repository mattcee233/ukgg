const moment = require("moment");
const uuid = require("uuid");
const bcrypt = require('bcrypt');
const emails = require('./emails');
const teamHandler = require("./teamHandler");

const login = async function (request,response,next){

    // //console.log(request.body);
    if(request.body.user && request.body.pass){


        // try to get user
        let [userResult,userResultFields] = await global.mysql.query("SELECT users.userID,userName,userPass,userKey,teams.teamID,teamName,teamAdmin,teamPass,validate,quizID,isActiveTeam FROM users LEFT JOIN userteams on users.userID=userteams.userID LEFT JOIN teams ON userteams.teamID=teams.teamID WHERE (userEmail=? OR userName=?);",[request.body.user,request.body.user]);

        if(userResult.findIndex(result=>{return result.isActiveTeam===1})!==-1){
            userResult = userResult.filter(result=>{return result.isActiveTeam===1});
        }

        // login correct, setup session
        if(userResult.length>0){
            console.log(request.body.user + " trying to log In");
            let pass = await bcrypt.compareSync(request.body.pass,userResult[0].userPass);
            delete userResult[0].userPass;
            // console.log(pass);
            // console.log(userResult[0].userPass);
            if(pass===true){
                console.log(request.body.user + " Logged In");
                
                // store session values
                request.session.loggedIn=true;
                Object.assign(request.session,userResult[0]);
                
                // request.session.userName=request.body.user;


                // set user as having been seen
                await global.mysql.query("UPDATE users SET lastSeen=NOW() WHERE userID=?;",userResult[0].userID);

                // get team detail where present
                let teamStatus = {};
                let teamUsers;
                let teamUsersFields;
                if(userResult[0].teamID!==null){
                    let [teamDetail,teamDetailFields] = await global.mysql.query("SELECT userName FROM users WHERE userID=?;",userResult[0].teamAdmin);
                    if(teamDetail.length>0){
                        request.session.teamAdminName=teamDetail[0].userName;
                        teamStatus.teamAdminName=teamDetail[0].userName;
                    }

                    [teamUsers,teamUsersFields] = await global.mysql.query("SELECT users.userID,lastSeen FROM users LEFT JOIN userteams ON users.userID=userteams.userID WHERE teamID=?;",userResult[0].teamID);
                    teamStatus.teamSize=teamUsers.length;
                    teamStatus.onlineMembers=teamUsers.filter(user=>moment(user.lastSeen).isAfter(moment().subtract(10,"minute"))).length;
                    if(teamStatus.onlineMembers===1){
                        await global.mysql.query("UPDATE teams SET teamAdmin=? WHERE teamID=?;",[request.session.userID,request.session.teamID]);
                        request.session.teamAdmin=request.session.userID;
                        request.session.teamAdminName=request.session.userName;
                    }
                    await teamHandler.republishTeam(request.session.teamID);
                }
                
                // return to client
                await next();
                // response.json({userID:request.session.userID,userName:request.session.userName,userKey:request.session.userKey,teamID:request.session.teamID,teamName:request.session.teamName,teamAdmin:request.session.teamAdmin,teamPass:request.session.teamPass,...teamStatus,teamAdminName:request.session.teamAdminName,validate:request.session.validate,quizID:request.session.quizID,vidLink:vidLink[request.session.quizID]});
                // response.json({...userResult[0],...teamStatus});
            }
            else{
                console.log("wrong password for "+request.body.user);
                response.sendStatus(401).end();
            }
        }
        else{
            // login failed, send 401 and close
            console.log(request.body.user + " doesnt seem to exist...");
            response.sendStatus(401).end(); // Bad Request
        }
    }
    else{
        response.sendStatus(401).end(); // Bad Request
    }
}

module.exports.login = login;

module.exports.register = async function register(request,response){
// //console.log(request.body);
    if(request.body.user && request.body.pass && request.body.email){
        request.body.email = request.body.email.trim();

        // request.body.user=request.body.user.replace("admin","");
        // request.body.user=request.body.user.replace("Admin","");
        // request.body.user=request.body.user.replace("ADMIN","");
        // request.body.user=request.body.user.replace("host","");
        // request.body.user=request.body.user.replace("Host","");
        // request.body.user=request.body.user.replace("HOST","");

        // check if username or email address already exists...
        let [userResult,userResultFields] = await global.mysql.query("SELECT userName,userEmail FROM users WHERE userName=? OR userEmail=?;",[request.body.user,request.body.email]);
        if(userResult.length>0){
            console.log("Duplicated Registration request - "+request.body.email+" as "+request.body.user + " (REJECTED)");
            // already exists, 501
            response.sendStatus(501).end(); // Not Implimented
        }
        else{
            console.log("Registration request - "+request.body.email+" as "+request.body.user);
            // doesn't exist, create user key and record
            let userKey = uuid.v4();
            let userHashedPass = await bcrypt.hash(request.body.pass,10);
            let [newUserResult,newUserResultFields] = await global.mysql.query("INSERT INTO users (userName,userEmail,userPass,lastSeen,userKey) VALUES (?,?,?,NOW(),?);",[request.body.user,request.body.email,userHashedPass,userKey]);
            // get the new user ID
            let userID = newUserResult.insertId;

            // login as new user
            request.session.loggedIn=true;
            request.session.userID=userID;
            request.session.userKey=userKey;
            request.session.userName=request.body.user;
            request.session.quizID=1;
            try{
                await emails.sendEmailConfirmation(request.body.email, userID, userKey);
            }
            catch(err){
                //console.log(err);
                await global.mysql.query("UPDATE users SET validate=1 WHERE userID=?;",userID);
            }
            // send back to client
            response.json({userID:userID,userKey:userKey,validate:0,quizID:request.session.quizID,vidLink:global.vidLink[request.session.quizID]});
        }
    }
    else{
        response.sendStatus(401).end(); // Bad Request
    }
}

module.exports.query = async function(request,response){
    // try to get user


    // login correct, setup session
    // if(userResult.length===1){


        console.log(request.session.userName + " re-connected...");
        // set user as having been seen
        await global.mysql.query("UPDATE users SET lastSeen=NOW() WHERE userID=?;",request.session.userID);

        // get team detail where present
        let teamStatus = {};
        let teamUsers;
        let teamUsersFields;
        if(request.session.teamID!==null && typeof request.session.teamID !== "undefined"){
            // //console.log(request.session.teamID);
            [teamUsers,teamUsersFields] = await global.mysql.query("SELECT users.userID,lastSeen FROM users LEFT JOIN userteams ON users.userID=userteams.userID WHERE teamID=? AND isActiveTeam=1;",request.session.teamID);
            teamStatus.teamSize=teamUsers.length;
            teamStatus.onlineMembers=teamUsers.filter(user=>moment(user.lastSeen).isAfter(moment().subtract(10,"minute"))).length;
            if(teamStatus.onlineMembers===1){
                await global.mysql.query("UPDATE teams SET teamAdmin=? WHERE teamID=?;",[request.session.userID,request.session.teamID]);
                request.session.teamAdmin=request.session.userID;
                request.session.teamAdminName=request.session.userName;
            }
        }

        await teamHandler.republishTeam(request.session.teamID);

        let quizStatus=[];
        let [questionStatus,questionStatusFields] = await global.mysql.query("SELECT questionID,roundNumber,questionNumber,question,img,isActive,closed,displayAnswer as actualAnswer FROM questions WHERE isAsked=1 AND quizID=?;",request.session.quizID);
        let suggestions,suggestionfields;
        let answerIndex;
        for(let question of questionStatus){
            
            // [suggestions,suggestionFields] = await global.mysql.query("SELECT teamanswer as answer,userNames as userName FROM suggestions WHERE questionID=? AND teamID=?;",[question.questionID,request.session.teamID]);
            let qSuggestions = global.suggestionCache.get(question.questionID);
            let teamSuggestions = [];
            if(qSuggestions!==undefined){
                // console.log(qSuggestions);
                // teamSuggestions = qSuggestions.find(sug=>sug.teamID===request.session.teamID);
                if(Object.keys(qSuggestions).includes(String(request.session.teamID))){
                    teamSuggestions=qSuggestions[String(request.session.teamID)];
                }
            }

            let suggestions = teamSuggestions.map(sug=>{return {answer:sug.answer,userName:sug.user}});
            // console.log(suggestions);
            question.answerMode=(question.closed===1);
            question.suggestions = suggestions;
            if(!question.answerMode){
                delete question.actualAnswer;
            }
            for (let suggestion of question.suggestions){
                suggestion.selected=false;
            }
            let [answers,answersFields] = await global.mysql.query("SELECT teamanswer as answer,correct FROM answers WHERE questionID=? AND teamID=?;",[question.questionID,request.session.teamID]);
            if(answers.length===1){
                if(!question.answerMode){
                    for(let answer of answers){
                        delete answer.correct;
                    }
                }
                else{
                    question.correct=answers[0].correct;
                    question.teamAnswer=answers[0].answer;
                }
                answerIndex=question.suggestions.findIndex(suggestion=>suggestion.answer===answers[0].answer);
                if(answerIndex!==-1){
                    question.suggestions[answerIndex].selected=true;
                    
                }
                else{
                    answers[0].selected=true;
                    question.suggestions = [answers[0],...question.suggestions];
                }
                
            }
            quizStatus[question.questionID]=question;
        }


        let [quizDetails,quizDetailsFields] = await global.mysql.query("SELECT * FROM quizzes WHERE quizID=?;",request.session.quizID);
        // //console.log(request.session);
        // return to client
        response.json({userID:request.session.userID,userName:request.session.userName,userKey:request.session.userKey,teamID:request.session.teamID,teamName:request.session.teamName,teamAdmin:request.session.teamAdmin,teamPass:request.session.teamPass,...teamStatus,teamAdminName:request.session.teamAdminName,validate:request.session.validate,quizID:request.session.quizID,vidLink:global.vidLink[request.session.quizID],quizStatus:quizStatus,quizDetail:quizDetails});
        // userID,userKey,teamID,teamName
    // }
    // else{
    //     // login failed, send 401 and close
    //     response.sendStatus(401).end();
    // }
}



module.exports.logout = async function(request,response){
    console.log(request.session.userName + " Logged Out");
    await global.mysql.query("UPDATE users SET lastSeen=? WHERE userID=?;",[moment().subtract(11,"minutes").toDate(),request.session.userID]);
    if(typeof request.session.teamID!=="undefined"){
        if(request.session.teamID!==null){
            await teamHandler.reallocateTeamAdmin(request.session.teamID);
        }
    }
    await request.session.destroy();
    response.sendStatus(200);
}

// const nodemailerTransport = nodemailer.createTransport({host:"smtp.ionos.co.uk",port:465,secure:true,auth:{user:"passwordreset@theukquarantinequiz.co.uk",pass:"Poiu9753$"}});


module.exports.validateEmail = async function(request,response){
    let [user,userFields] = await global.mysql.query("SELECT * FROM users WHERE userKey=? AND userID=?;",[request.query.vkey,request.query.uid]);
    if(user.length===1){
        console.log("Validating email address for "+user[0].userName);
        let newKey=uuid.v4();
        await global.mysql.query("UPDATE users SET validate=1,userKey=? WHERE userID=?;",[newKey,request.query.uid]);
        request.session.userKey=newKey;
        response.redirect("https://theukquarantinequiz.co.uk?validate=done");
    }
    else{
        response.redirect("https://theukquarantinequiz.co.uk");
    }

}




module.exports.forgottenPass = async function(request,response){
    if(request.body.userName){
        let [userRec,userRecFields] = await global.mysql.query("SELECT * FROM users WHERE userName=? OR userEmail=?;",[request.body.userName,request.body.userName]);
        if(userRec.length>0){
            // //console.log(userRec);
            console.log("sending reset email to "+userRec[0].userName);
            try{
                await emails.sendPasswordReset(userRec[0].userEmail, userRec[0].userKey);
            }
            catch(err){
                console.log(err);
            }
            response.sendStatus(200).end();
        }
        else{
            response.sendStatus(404).end();
        }
    }
}

module.exports.changePass = async function(request,response){
    console.log("password change request");
    if(request.body.userKey && request.body.pass){
        let [user,userFields] = await global.mysql.query("SELECT * FROM users WHERE userKey=?;",request.body.userKey);
        if(user.length===1){
            let newPass=await bcrypt.hash(request.body.pass,10);
            let key = request.body.userKey;
            request.body.userName=user.userName;
            let newKey = uuid.v4();

            await global.mysql.query("UPDATE users SET userPass=?,userKey=? WHERE userID=?;",[newPass,newKey,user[0].userID]);
            response.json({userName:user[0].userName});

        }
        else{
            response.sendStatus(400).end();
        }

    }
    else{
        response.sendStatus(400).end();
    }
}
