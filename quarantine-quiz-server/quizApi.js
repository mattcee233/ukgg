const NodeCache = require("node-cache");
const moment = require("moment");
const formidable = require('formidable');
const fs = require('fs').promises;
const path = require('path');
const uuid = require("uuid");
// global.questionCache = new NodeCache();




module.exports.chatter = async function(request,response){
    // //console.log(request.body);
    if(request.body.message){
        request.body.message=request.body.message.substring(0,250);
        
        // //console.log(request.body.channel);
        // //console.log(request.body);
        // if(request.session.userID===request.body.userID && request.body.userKey===request.session.userKey){
            let channel=request.session.teamID;
            let channelName=request.session.teamName;
            let userName=request.session.userName;
            // //console.log(channel);
            // let [adminKey,adminKeyFields] = await global.mysql.query("SELECT * FROM users LEFT JOIN userteams ON users.userID=userteams.userID WHERE isAdmin=1 AND isActiveTeam=1;");
            // let adminRecord = adminKey.find(admin=>admin.userID===request.session.userID && admin.userKey===request.session.userKey);
            if(global.adminKeysList.findIndex(a=>a.userID===request.session.userID)!==-1){
                
                let adminRecord = global.adminKeysList.find(a=>a.userID===request.session.userID);

                // console.log(adminRecord);
            // if(request.session.userID===1){
                channel = request.body.channel || adminRecord.defaultTeam;
                if(typeof request.body.channel !=="undefined"){
                    let [teamInfo,teamInfoFields] = await global.mysql.query("SELECT * FROM teams WHERE teamID=?;",channel);
                    channelName=teamInfo[0].teamName;
                }

                if(adminRecord.userName==="ThreePhase"){

                    userName = request.session.quizID===1?"ThreePhase (Host)":"ThreePhase (Admin)";
                }
                else if(adminRecord.userName==="meganelady"){
                    userName = request.session.quizID===1?"@Wife (Admin)":"MeganeLady (Admin)";
                }
                else{
                    userName=adminRecord.userName + " (Admin)";
                }
                
            }
            // //console.log(channel);
            // //console.log("auth key confirmed, publishing to team "+ request.session.teamID);
            let message={
                quizID:request.session.quizID,
                user: userName,
                teamName:channelName,
                text:request.body.message,
                timestamp:moment().format(),
                channel:channel,
                ext:{serverKey:global.serverKey}
            };
            // console.log(message);
            response.sendStatus(200).end(); // OK
            console.log("-- " + message.teamName+"#"+message.channel+" | " + message.user + " : "+message.text);
            global.fayeClient.publish("/"+message.quizID+"/chat/"+channel,message);
            
        // }
        
    }
    else{
        // //console.log("message rejected due to mismatching user key.")
        response.sendStatus(400).end(); // Not Authorised
    }
}

const answer = async function (request,response){
    // //console.log(request.body);

    if(request.session.teamAdmin!==request.session.userID){
        suggest(request,response);
    }
    else{

        if(request.body.answer && !isNaN(Number(request.body.questionID))){
            request.body.answer=capitaliser(request.body.answer.substring(0,100));
            console.error("Incoming ANSWER from " + request.session.userName + " of team '"+request.session.teamName+"' | QID:"+request.body.questionID+" = " + request.body.answer);
            
            // console.log(request.session);
            if(request.session.userID===request.session.teamAdmin){
                // //console.log(request.body.answer);
                try{
                    let suggestionMessage={
                        quizID:request.session.quizID,
                        questionID:request.body.questionID,
                        user:request.session.userName,
                        answer:request.body.answer,
                        team:request.session.teamName,
                        teamID:request.session.teamID,
                        selected:true,
                        // timestamp:request.body.timestamp,
                        ext:{serverKey:global.serverKey}
                    };

                    let shouldSend=true;


                    let currentQuestionSuggestions = global.suggestionCache.get(request.body.questionID);
                    if(currentQuestionSuggestions===undefined){
        
                        // console.log("team suggestion not found")
                        currentQuestionSuggestions = {};
                    }
                    
        
                    let currentTeamSuggestions = [];
                    
                    // console.log(Object.keys(currentQuestionSuggestions));
                    // console.log(request.session.teamID);
        
                    if(Object.keys(currentQuestionSuggestions).includes(String(request.session.teamID))){
                        // console.log("found team in object...");
                        // check for answer, update suggestions etc if required
                        currentTeamSuggestions = currentQuestionSuggestions[String(request.session.teamID)];
        
                        // console.log(currentTeamSuggestions);
                        let suggestionIndex = currentTeamSuggestions.findIndex(sug=>sug.answer===request.body.answer);
                        // console.log(suggestionIndex);
                        let currentSuggestion;
                        if(suggestionIndex!==-1){
                            currentSuggestion = currentTeamSuggestions[suggestionIndex];

                            if(currentSuggestion.selected===true){
                                shouldSend=false;
                            }
                            // console.log(currentSuggestion);
                            if(currentSuggestion.user.includes(request.session.userName)===false){
                                console.log("not in there! adding!");
                                currentSuggestion.user = currentSuggestion.user + ", "+request.session.userName;
                                suggestionMessage.user = currentSuggestion.user;
                            }
                            else{
                                suggestionMessage.user = currentSuggestion.user;
                            }
                            // console.log(currentSuggestion);
                            currentTeamSuggestions[suggestionIndex] = currentSuggestion;
        
                        }
                        else{
                            currentSuggestion = suggestionMessage;
                            // console.log(currentTeamSuggestions);
                            currentTeamSuggestions.push(currentSuggestion);
                        }
                    }
                    else{
                        // add suggestion
                        currentTeamSuggestions.push(suggestionMessage);
                    }

                    currentQuestionSuggestions[String(request.session.teamID)] = currentTeamSuggestions;
                    // console.log(currentQuestionSuggestions);
        
                    global.suggestionCache.set(request.body.questionID,currentQuestionSuggestions);

                    // if(shouldSend===true){
                        let answerID;
                    // if(existingAnswer.length>0){
                    //     answerID=existingAnswer[0].answerID;
                    //     if(existingAnswer[0].teamanswer!==suggestionMessage.answer){
                    //         await global.mysql.query("UPDATE answers SET teamanswer=?, correct=0,marked=0 WHERE answerID=?;",[suggestionMessage.answer,answerID]);
                    //     }
                    // }
                    // else{
                        let [answerIDQuery,answerIDQueryFields] = await global.mysql.query("INSERT INTO answers(teamID,questionID,quizID,teamanswer) VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE teamanswer=?,correct=0,marked=0;",[suggestionMessage.teamID,request.body.questionID,request.session.quizID,suggestionMessage.answer,suggestionMessage.answer]);
                        
                        answerID=answerIDQuery.insertId;

                        global.fayeClient.publish("/"+suggestionMessage.quizID+"/suggestion/"+suggestionMessage.teamID,suggestionMessage);
                    // }
                    response.sendStatus(200).end(); // OK

                    

                    
                }
                catch(err){
                    console.error(err);
                    response.sendStatus(500).end();
                }
                // let [questionID,questionIDFields] = await global.mysql.query("SELECT questionID FROM questions WHERE roundNumber = 1 AND questionNumber = 1;",[request.body.roundNumber,request.body.questionNumber]);
                
                    // let [existingAnswer,existingAnswerFields] = await global.mysql.query("SELECT * FROM answers WHERE teamID=? AND questionID=? AND quizID=?;",[suggestionMessage.teamID,request.body.questionID,request.session.quizID]);
                    
                    // }

                    
                    // confirmAnswer(answerID);
                    
                
                
                
                
            }
            else{
                response.sendStatus(401).end();
            }
            
                
            
            
        }
        else{
            //console.log("message rejected due to mismatching user key.")
            response.sendStatus(401).end(); // Not Authorised
        }
    }
}

module.exports.answer = answer;

global.suggestionCache = new NodeCache();

const capitaliser = function capitaliser(str){
    return str.toLowerCase().replace(/(?:^|\s|["'([{])+\S/g, match => match.toUpperCase());
}

const suggest = async function(request,response){
    // //console.log(request.body);
    try{
        let goToAnswerDirect=false;
        if(request.session.teamAdmin===request.session.userID){
            let [teamUsers] = await global.mysql.query("SELECT users.userID,userName,lastSeen FROM users LEFT JOIN userteams on users.userID = userteams.userID WHERE teamID=? AND userteams.isActiveTeam=1;",request.session.teamID);
            teamUsers = teamUsers.filter(user=>{return moment(user.lastSeen).isAfter(moment().subtract(5,"minute"))});
            gotToAnswerDirect = (teamUsers.length===1);
        }


        if(goToAnswerDirect===true){
            answer(request,response);
        }
        else{

            if(request.body.answer && request.session.teamID){
                // //console.log("incoming answer suggestion from " + request.session.userName);
                try{
                    request.body.answer = capitaliser(request.body.answer.substring(0,100));

                
                    // //console.log(request.body);
                    console.log("Incoming suggestion from " + request.session.userName + " of team '"+request.session.teamName+"' | QID:"+request.body.questionID+" = " + request.body.answer);
                    let shouldSend = true;

                    let suggestionMessage={
                        quizID:request.session.quizID,
                        questionID:request.body.questionID,
                        user:request.session.userName,
                        answer:request.body.answer,
                        // roundNumber:request.body.roundNumber,
                        // questionNumber:request.body.questionNumber,
                        team:request.session.teamName,
                        teamID:request.session.teamID,
                        selected:false,
                        // timestamp:request.body.timestamp,
                        ext:{serverKey:global.serverKey}
                    };

                    //get question from cache
                    let currentQuestionSuggestions = global.suggestionCache.get(request.body.questionID);
                    if(currentQuestionSuggestions===undefined){

                        // console.log("team suggestion not found")
                        currentQuestionSuggestions = {};
                    }
                    

                    let currentTeamSuggestions = [];
                    
                    // console.log(Object.keys(currentQuestionSuggestions));
                    // console.log(request.session.teamID);

                    if(Object.keys(currentQuestionSuggestions).includes(String(request.session.teamID))){
                        // console.log("found team in object...");
                        // check for answer, update suggestions etc if required
                        currentTeamSuggestions = currentQuestionSuggestions[String(request.session.teamID)];

                        // console.log(currentTeamSuggestions);
                        let suggestionIndex = currentTeamSuggestions.findIndex(sug=>sug.answer===request.body.answer);
                        // console.log(suggestionIndex);
                        let currentSuggestion;
                        if(suggestionIndex!==-1){
                            currentSuggestion = currentTeamSuggestions[suggestionIndex];

                            if(currentSuggestion.selected===true){
                                currentSuggestion.selected=false;
                            }

                            // console.log(currentSuggestion);
                            if(currentSuggestion.user.includes(request.session.userName)===false){
                                // console.log("not in there! adding!");
                                currentSuggestion.user = currentSuggestion.user + ", "+request.session.userName;
                                suggestionMessage.user = currentSuggestion.user;
                            }
                            else{
                                shouldSend=false;
                            }
                            // console.log(currentSuggestion);
                            currentTeamSuggestions[suggestionIndex] = currentSuggestion;

                        }
                        else{
                            currentSuggestion = suggestionMessage;
                            // console.log(currentTeamSuggestions);
                            currentTeamSuggestions.push(currentSuggestion);
                        }
                    }
                    else{
                        // add suggestion
                        currentTeamSuggestions.push(suggestionMessage);
                    }

                    currentQuestionSuggestions[String(request.session.teamID)] = currentTeamSuggestions;
                    // console.log(currentQuestionSuggestions);

                    global.suggestionCache.set(request.body.questionID,currentQuestionSuggestions);

                    
                    

                    
                    // //console.log("sending answer suggestion to team "+ request.session.teamID);
                    if(shouldSend===true){
                        global.fayeClient.publish("/"+suggestionMessage.quizID+"/suggestion/"+suggestionMessage.teamID,suggestionMessage);
                    }



                    // let [existingSuggestion,existingSuggestionFields] = await global.mysql.query("SELECT * FROM suggestions WHERE teamID=? AND questionID=? AND quizID=? AND teamanswer=?;",[suggestionMessage.teamID,request.body.questionID,request.session.quizID,suggestionMessage.answer]);
                    // if(existingSuggestion.length===0){
                    //     await global.mysql.query("INSERT INTO suggestions(teamID,questionID,quizID,teamanswer,userNames) VALUES (?,?,?,?,?);",[suggestionMessage.teamID,request.body.questionID,request.session.quizID,suggestionMessage.answer,request.session.userName]);
                    // }
                    // else{
                    //     if(!existingSuggestion[0].userNames.includes(request.session.userName)){
                    //         await global.mysql.query("UPDATE suggestions SET userNames=? WHERE suggestionID=?;",[existingSuggestion[0].userNames+" & "+request.session.userName,existingSuggestion[0].suggestionID]);
                    //     }
                    // }

                    response.sendStatus(200).end(); // OK
                }
                catch(err){
                    console.error(err);
                    response.sendStatus(500).end();
                }

                
            }
            else{
                // //console.log("no answer?");
                response.sendStatus(400).end(); // Not Authorised
            }
        }
    }
    catch(err){
        console.log(err);
        response.sendStatus(500).end();
    }
}
module.exports.suggest = suggest;


module.exports.leaderboard = async function(request,response){
    let [leaderboard,leaderboardFields] = await global.mysql.query("SELECT sum(questions.pointValue*answers.correct) as score,teams.teamID,teams.teamName FROM answers LEFT JOIN teams on answers.teamID=teams.teamID LEFT JOIN questions ON questions.questionID=answers.questionID WHERE answers.quizID=? GROUP BY teamID ORDER BY score DESC;",request.session.quizID);
    response.json(leaderboard);
} 

module.exports.sendLeaderBoard = async function(request,response){
    let [leaderboard,leaderboardFields] = await global.mysql.query("SELECT sum(questions.pointValue*answers.correct) as score,teams.teamID,teams.teamName FROM answers LEFT JOIN teams on answers.teamID=teams.teamID LEFT JOIN questions ON questions.questionID=answers.questionID WHERE answers.quizID=? GROUP BY teamID ORDER BY score DESC,teamName;",request.session.quizID);
    //console.log(leaderboard);
    //console.log(request.session.quizID);
    global.fayeClient.publish("/"+request.session.quizID+"/leaderboard",{data:leaderboard,quizID:request.session.quizID,ext:{serverKey:global.serverKey}});
    response.sendStatus(200).end();
}

module.exports.clearLeaderboard = async function(request,response){
    await global.mysql.query("DELETE FROM answers WHERE quizID=?;",request.session.quizID);
    // await global.mysql.query("DELETE FROM suggestions WHERE quizID=?;",request.session.quizID);
    response.sendStatus(200).end();
}



module.exports.getQuizzes = async function(request,response){
    let [quizInfo,quizInfoFields] = await global.mysql.query("SELECT * FROM quizzes");
    response.json(quizInfo);
}


module.exports.getQuiz = async function(request,response){
    // if(request.query.quizID){
    //     if(!isNaN(Number(request.query.quizID))){

            let [quizDetail,quizDetailFields] = await global.mysql.query("SELECT * FROM questions WHERE quizID=? ORDER BY roundNumber,questionNumber;",request.session.quizID);
            if(quizDetail.length>0){
                //console.log("Send quiz details to host...");
                response.json(quizDetail);
            }
            else{
                response.sendStatus(404).end();
            }
            
    //     }
    //     else{
    //         response.sendStatus(400).end();
    //     }
    // }
    // else{
    //     response.sendStatus(400).end();
    // }
}

module.exports.setQuestion = async function(request,response){
    //console.log(request.body);
    delete request.body.img;
    await global.mysql.query("UPDATE questions SET ? WHERE questionID=?;",[request.body,Number(request.body.questionID)]);
    response.sendStatus(200);
    // if(request.body.roundNumber && request.body.questionNumber && request.body.question && request.body.answerType && request.body.answer){
    //     await global.mysql.query("INSERT INTO questions(roundNumber,questionNumber,question,answerType,answer,img) VALUES (?,?,?,?,?,?);",[request.body.roundNumber, request.body.questionNumber, request.body.question, request.body.answerType, request.body.answer,request.body.img]);
    //     response.sendStatus(200).end();
    // }
    // else{
    //     response.sendStatus(400).end();
    // }
}


module.exports.newQuestion = async function(request,response){
    //console.log(request.body);
    let [newQID,newQIDFields] = await global.mysql.query("INSERT INTO questions SET ?;",{quizID:request.session.quizID,roundNumber:request.body.roundNumber,questionNumber:request.body.questionNumber});
    response.json({questionID:newQID.insertId});
}

module.exports.sendQuestion = async function(request,response){
    if(request.query.questionID){
        let qID=Number(request.query.questionID);
        
        let [question,questionFields] = await global.mysql.query("SELECT * FROM questions WHERE questionID=?;",qID);
        let quizID=request.session.quizID;
        if(question.length===1){
            response.sendStatus(200).end();
            let answerMode=false;
            if(typeof request.query.a!=="undefined"){
                
                answerMode = (Number(request.query.a)===1);

            }
            if (answerMode){
                console.log("---------------------- SENDING OUT ANSWER TO QUESTION "+request.query.questionID+" ----------------------");
                await global.mysql.query("UPDATE questions SET isActive=0 WHERE quizID=?;",quizID);
                await global.mysql.query("UPDATE questions SET isActive=1,closed=1,isAsked=1 WHERE questionID=?;",qID);
                // await global.mysql.query("UPDATE questions SET closed=1 WHERE questionID=?;",qID);
                let [answerList,answerListFields] = await global.mysql.query("SELECT teams.*,answerFiltered.teamanswer,answerFiltered.correct FROM teams LEFT JOIN (SELECT * FROM answers WHERE questionID=? AND quizID=?) answerFiltered ON teams.teamID=answerFiltered.teamID;",[qID,quizID]);
                // //console.log(answerList);
                for (let team of answerList){
                    // //console.log(team);
                    global.fayeClient.publish("/"+quizID+"/answer/"+team.teamID,{
                        quizID:quizID,
                        questionID:question[0].questionID,
                        roundNumber:question[0].roundNumber,
                        questionNumber:question[0].questionNumber,
                        question:question[0].question,
                        img:question[0].img,
                        answerMode:true,
                        actualAnswer: question[0].displayAnswer,
                        teamAnswer: team.teamanswer || false,
                        correct: team.correct || 0,
                        ext:{
                            serverKey:global.serverKey
                        }
                    });
                }

                global.suggestionCache.del(question[0].questionID);
                
            }
            else{
                console.log("---------------------- SENDING OUT QUESTION "+request.query.questionID+" ----------------------");
                await global.mysql.query("UPDATE questions SET isActive=0 WHERE quizID=?;",quizID);
                await global.mysql.query("UPDATE questions SET isActive=1,isAsked=1 WHERE questionID=? AND quizID=?;",[qID,quizID]);
                global.fayeClient.publish("/"+quizID+"/question",{
                    quizID:quizID,
                    questionID:question[0].questionID,
                    roundNumber:question[0].roundNumber,
                    questionNumber:question[0].questionNumber,
                    question:question[0].question,
                    img:question[0].img,
                    answerMode:false,
                    
                    ext:{
                        serverKey:global.serverKey
                    }
                });
                // global.fayeClient.publish("/question",{
                //     questionID:question[0].questionID,
                //     roundNumber:question[0].roundNumber,
                //     questionNumber:question[0].questionNumber,
                //     question:question[0].question,
                //     img:question[0].img,
                //     answerMode:answerMode,
                //     actualAnswer: (answerMode?question[0].displayAnswer:undefined),
                //     ext:{
                //         serverKey:global.serverKey
                //     }
                // });
            }
            
            if(question[0].img===null){
                delete question[0].img;
            }
            
            
            
        }
        else{
            response.sendStatus(400).end();
        }
        
    }
    else{
        response.sendStatus(400).end();
    }
    
}

global.vidLink={1:"logo",2:"logo",3:"logo"};

module.exports.setVidLink = async function(request,response){
    // //console.log(request.body)
    if(request.body.vidLink){
        if(request.body.vidLink!=="reload"){
            vidLink[request.session.quizID]=request.body.vidLink;    
        }
        
        global.fayeClient.publish("/"+request.session.quizID+"/vid",{
            quizID:request.session.quizID,
            vidLink:request.body.vidLink,
            ext:{
                serverKey:global.serverKey
            }
        });
        response.sendStatus(200).end();
    }
    else{
        response.sendStatus(400).end();
    }
    
}

module.exports.getVidLink = async function(request,response){
    if(request.session.quizID){
        if(typeof global.vidLink[request.session.quizID]!=="undefined"){
            response.json({vidLink:vidLink[request.session.quizID]});
        }
        else{
            response.json({vidLink:"logo"});
        }
    }
    else{
        response.json({vidLink:vidLink[1]});
    }
}


module.exports.forceCorrect = async function(request,response){
    if(request.query.answerID){
        if(!isNaN(Number(request.query.answerID))){
            await global.mysql.query("UPDATE answers SET correct=1,marked=1 WHERE answerID=?;",request.query.answerID);
            let [answerList,answerListFields] = await global.mysql.query("SELECT * FROM teams LEFT JOIN answers ON teams.teamID=answers.teamID WHERE answerID=?;",request.query.answerID);
            let [question,questionFields] = await global.mysql.query("SELECT * FROM questions WHERE questionID=?;",answerList[0].questionID);
            if(question.length>0 && answerList.length>0){
                if(question[0].closed===1){
                
                
                    global.fayeClient.publish("/"+request.session.quizID+"/host/answers",{data:[{
                        quizID:request.session.quizID,
                        questionID:question[0].questionID,
                        roundNumber:question[0].roundNumber,
                        questionNumber:question[0].questionNumber,
                        question:question[0].question,
                        img:question[0].img,
                        answerMode:true,
                        actualAnswer: question[0].displayAnswer,
                        teamAnswer: answerList[0].teamanswer,
                        correct:answerList[0].correct,
                        
                    }],
                    ext:{
                        serverKey:global.serverKey
                    }});

                    global.fayeClient.publish("/"+request.session.quizID+"/answer/"+answerList[0].teamID,{
                        quizID:request.session.quizID,
                        questionID:question[0].questionID,
                        roundNumber:question[0].roundNumber,
                        questionNumber:question[0].questionNumber,
                        question:question[0].question,
                        img:question[0].img,
                        answerMode:true,
                        actualAnswer: question[0].displayAnswer,
                        teamAnswer: answerList[0].teamanswer,
                        correct:answerList[0].correct,
                        ext:{
                            serverKey:global.serverKey
                        }
                    });
                
                }
            }



            response.sendStatus(200).end();
        }
        else{
            response.sendStatus(400).end();
        }
    }
    else{
        response.sendStatus(400).end();
    }
    
}



module.exports.forceRemark = async function(request,response){
    if(request.query.questionID){
        if(!isNaN(Number(request.query.questionID))){
            await global.mysql.query("UPDATE answers SET correct=0,marked=0 WHERE questionID=?;",request.query.questionID);
            



            response.sendStatus(200).end();
        }
        else{
            response.sendStatus(400).end();
        }
    }
    else{
        response.sendStatus(400).end();
    }
    
}

let roundNumber=1;
let questionNumber=0;
module.exports.fakeQuestions = function(delay,quizID){
    
    setInterval(()=>{
        
        questionNumber++;
        if(questionNumber>10){
            questionNumber=1;
            roundNumber++;
        }
        
        //console.log("sending fake question "+roundNumber+"-"+questionNumber);
        global.fayeClient.publish("/"+quizID+"/question",{
            quizID:quizID,
            roundNumber:roundNumber,
            questionNumber:questionNumber,
            question:"fake question "+roundNumber+"-"+questionNumber,
            answerMode:false,
            img:questionNumber%2===0 ? "https://picsum.photos/"+Math.round(Math.random()*600)+"/"+Math.round(Math.random()*600) : undefined,
            ext:{
                serverKey:global.serverKey
            }
        });
        
    }
    ,delay);
}


module.exports.switchQuiz = async function(request,response){
    console.log(request.session.userName + " trying to swap quizzes from quizID "+request.session.quizID+" to "+request.query.quizID);
    try{
        console.log(request.session.userName + " Swapped quizzes from quizID "+request.session.quizID+" to "+request.query.quizID);
        request.session.quizID=Number(request.query.quizID);
        
        await global.mysql.query("UPDATE users SET quizID=? WHERE userID=?;",[request.query.quizID,request.session.userID]);
    }
    catch(err){

        console.error(request.session.userName + " Swapped quizzes from quizID "+request.session.quizID+" to 1 as result of an exception");
        request.session.quizID=1;
        await global.mysql.query("UPDATE users SET quizID=? WHERE userID=?;",[1,request.session.userID]);
    }
    response.redirect(301,"https://theukquarantinequiz.co.uk");
}


module.exports.newQuiz = async function(request,response){
    
    // let newQuizID=Number(moment().format("MMDDhh")+request.session.userID+request.session.quizID);
    try{
        let newQuizID;
        let existingQuiz;
        // console.log(newQuizID);
        do{
            newQuizID=Math.floor((Math.random()*1147483647)+(100000000-1));
            [existingQuiz] = await global.mysql.query("SELECT * FROM questions WHERE quizID=?;",[newQuizID]);
        } 
        while(existingQuiz.length>1);
        
        await global.mysql.query("UPDATE questions SET quizID=? WHERE quizID=?;",[newQuizID,request.session.quizID]);
        await global.mysql.query("UPDATE answers SET quizID=? WHERE quizID=?;",[newQuizID,request.session.quizID]);
        console.log(request.session.userName + " has archived quiz "+request.session.quizID+" to quizID "+newQuizID);
    }

    catch(err){
        console.error("Error whilst trying to archive a quiz");
        console.error(request.session);
        console.error(err);
    }


    response.sendStatus(200);

}


module.exports.saveFile = async function(request,response){
    try{
        let form = new formidable.IncomingForm();
        form.hash = "md5";
        form.keepExtensions = true;
        let newName;
        await form.parse(request,async (err,fields,files)=>{
            
            if(err){throw err;}
            //console.log(fields);
            //console.log(files);
            // //console.log(files.file.path);
            

            // let job = request.query.jobId;
            // logger.info("File upload processing",{file:files.filetoupload.name,job:job,session:request.session,user:request.session.userName})

            // let [groupNumber,groupNumberFields] = await dbPool.poolSystem.query("SELECT FKtogaGroups FROM tnccjobs WHERE tnccJobsID = ?;",job);

            // let groupFK = groupNumber[0].FKtogaGroups;
            let fileName = files.file.name;
            newName = await uuid.v4().substring(0,12).replace("-","")+fileName.replace(/[^\w.]/g,"");
            let tmpPath = files.file.path;
            // let newChecksum = files.filetoupload.hash;
            await fs.rename(path.join(tmpPath),path.join(process.env.saveFilePath,newName));
            //console.log("Saved "+fileName+" to storage dir as "+newName);
            newName="/img/"+newName;
            await global.mysql.query("UPDATE questions SET img=? WHERE questionID=?;",[newName,Number(fields.questionID)]);
            // await syncAttachFile(tmpPath,fileName,groupFK,files.filetoupload.lastModifiedDate,newChecksum,"USER");
            
            // let [jobList,jobListFields] = await dbPool.poolSystem.query("SELECT tnccjobs.* FROM tnccjobs WHERE tnccjobs.FKtogaGroups = ?;",groupFK);
            // for (let job of jobList){
            //     await fayeFunctions.republishJob(job.tnccJobsID);
            // }
        });


        


        response.json({fileName:newName});

        


    }
    catch(err){
        //console.log(err);
        // logger.error("File upload handler error",{requestId:request.id,session:request.session,err:err,errMessage:err.message,stack:err.stack});
        response.sendStatus(500);
    }
}
