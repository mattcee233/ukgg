const uuid = require('uuid');
const authHandler = require("./authHandler");
const moment = require("moment");

module.exports.createTeam = async function(request,response,next){
    // //console.log(request.body);
    if(request.body.teamName){
        let teamPass=uuid.v4().substring(0,16);
        console.log("team creation");
        console.log(request.body);
        let [teamID,teamIDFields] = await global.mysql.query("INSERT INTO teams (teamName,teamAdmin,teamPass,teamDesc) VALUES (?,?,?,?);",[request.body.teamName,request.session.userID,teamPass,request.body.teamDesc]);
        // await global.mysql.query("UPDATE users SET userTeam=? WHERE userID=?;",[teamID.insertId,request.session.userID]);
        let teamData = await addUserToTeam(request.session.userID,teamID.insertId,teamPass);
        console.log(request.session.userName + " created team '"+request.body.teamName+"#"+teamID.insertId+"' ("+request.body.teamDesc+")");
        request.session.teamID=teamID.insertId;
        request.session.teamName=request.body.teamName;
        request.session.teamPass=teamPass;
        request.session.teamAdmin=request.session.userID;
        request.session.teamAdminName=request.session.userName;
        console.log(request.session);
        next();
        // response.json(teamData);
    }
    else{
        response.sendStatus(400).end(); // Bad Request
    }
}



const addUserToTeam = async function(userID,teamID,teamPass){
    // //console.log(request.body);
    try{
        let [teamDetail,teamDetailFields] = await global.mysql.query("SELECT * FROM teams LEFT JOIN users ON teams.teamAdmin = users.userID WHERE teamID=? AND teamPass=?;",[teamID,teamPass]);
        console.log(userID+" trying to join team  "+teamID+" : "+teamPass);
        if(teamDetail.length>0){

            // console.log("User " + userID + " joining team "+teamDetail[0].teamName + " using a join key");

            // check if user is already in team before trying to switch them to it, if they aren't then make them join team first :)
            let [userInTeam,userInTeamFields] = await global.mysql.query("SELECT * FROM userteams WHERE userID=? AND teamID=?",[userID,teamID]);
            // //console.log(userInTeam);
            if(userInTeam.length===0){
                await global.mysql.query("INSERT INTO userteams(userID,teamID) VALUES (?,?);",[userID,teamID]);
                console.log("User " + userID + " joining team "+teamDetail[0].teamName + " using a join key");
            }
            else{
                console.log("User " + userID + " swapped teams to "+teamDetail[0].teamName);
            }

            //console.log("setting active team to "+teamID);

            await global.mysql.query("UPDATE userteams SET isActiveTeam=IF(teamID=?,1,0) WHERE userID=?;",[teamID,userID]);
            await global.mysql.query("UPDATE users SET teamUpdate=0 WHERE userID=?;",userID);



            return {
                teamID:teamID,
                teamPass:teamPass,
                teamName:teamDetail[0].teamName,
                teamAdmin:teamDetail[0].teamAdmin,
                teamAdminName:teamDetail[0].userName,
                teamDesc:teamDetail[0].teamDesc
            };
            
        }
        else{
            throw new Error("Team Not Found or Pass Incorrect");
        }
    }
    catch(err){

        console.error("teamHandler -> addUserToTeam -> General");
        console.error(err);
        
        return false;
    }

}

const joinTeam = async function(request,response,next){
    // //console.log(request.body);
    // if(typeof request.session.teamID==="undefined" || request.session.teamID===null){
        if(request.body.teamID && request.body.teamPass && !isNaN(Number(request.body.teamID))){
            teamID=Number(request.body.teamID);
            let joinResult = await addUserToTeam(request.session.userID,teamID,request.body.teamPass);
            if(joinResult!==false){
                request.session.teamID=teamID;
                request.session.teamPass=request.body.teamPass;
                request.session.teamName=joinResult.teamName;
                request.session.teamAdmin=joinResult.teamAdmin;
                request.session.teamAdminName=joinResult.teamAdminName;
                // //console.log(request.session);
                next();
            }
            else{
                response.sendStatus(409).end(); // Conflict
            }
        }
        else{
            response.sendStatus(400).end(); // Bad Request
        }
    // }
    // else{
    //     await changeTeam(request,response);
    // }

}
module.exports.joinTeam = joinTeam;


const getTeam = async function(request,response){
    if(request.session.teamID){
        let [teamDetail,teamDetailFields] = await global.mysql.query("SELECT teamName,teamDesc,teamPass,teamAdmin,userName as teamAdminName FROM teams LEFT JOIN users ON teams.teamAdmin=users.userID WHERE teamID=?;",request.session.teamID);
        if(teamDetail.length>0){
            let teamStatus = {teamName:teamDetail[0].teamName,teamPass:teamDetail[0].teamPass,teamDesc:teamDetail[0].teamDesc,teamAdmin:teamDetail[0].teamAdmin,teamAdminName:teamDetail[0].teamAdminName};
            let [teamUsers,teamUsersFields] = await global.mysql.query("SELECT users.userID,userName,lastSeen FROM users LEFT JOIN userteams on users.userID = userteams.userID WHERE teamID=? AND userteams.isActiveTeam=1;",request.session.teamID);
            try{
                // try{
                // console.log(teamUsers);
                // console.log(moment(teamUsers.find(user=>{return user.userID===teamDetail[0].teamAdmin}).lastSeen));
                
                // console.log(moment().subtract(10,"minutes"));
                // console.log(moment(teamUsers.find(user=>{return user.userID===teamDetail[0].teamAdmin}).lastSeen).isBefore(moment().subtract(10,"minutes")))
                
                // }
                // catch(err){
                //     console.log(err);
                // }
                if(moment(teamUsers.find(user=>{return user.userID===teamDetail[0].teamAdmin}).lastSeen).isBefore(moment().subtract(10,"minutes"))){
                    
                    await reallocateTeamAdmin(request.session.teamID);
                    await getTeam(request,response);
                    // response.sendStatus(200).end();
                }
                else{
                    response.json({...teamStatus,members:teamUsers});
                }
            }
            catch(err){
                response.json({...teamStatus,members:teamUsers});
                console.log(err);
            //     await getTeam(request,response);
            }
            
            
            
        }
        else{
            response.sendStatus(404).end();
        }
    }
    else{
        response.sendStatus(400).end(); // Bad Request
    }
}

module.exports.getTeam = getTeam;

const getTeamList = async function(userID){
    let [teamList,teamListFields] = await global.mysql.query("SELECT * FROM teams WHERE teamID IN (SELECT teamID FROM userteams WHERE userID=?);",userID);
    return teamList;
}
module.exports.getTeamList = getTeamList;

module.exports.sendTeamList = async function(request,response){
    //console.log("get team list for user "+request.session.userID);
    let teamList = await getTeamList(request.session.userID);
    // //console.log(teamList);
    response.json(teamList);
}


module.exports.changeName = async function(request,response){
    
    // //console.log(request.body);
    
    if(request.body.teamName && typeof request.session.teamID!=="undefined" && request.session.teamID!==null){
        console.log(request.session.userName + " changed their team name from '"+request.session.teamName + "' to '"+request.body.teamName+"'");
        await global.mysql.query("UPDATE teams SET teamName=? WHERE teamID=?;",[request.body.teamName,request.session.teamID]);
        await global.mysql.query("UPDATE users SET teamUpdate=1 WHERE userID IN (SELECT userID FROM userteams WHERE teamID=?);",request.session.teamID);
        request.session.teamName=request.body.teamName;
        await republishTeam(request.session.teamID);
        response.sendStatus(200).end();
    }
    else{
        console.error(request.session.userName + " tried to update their team name... it didn't seem to work though...");
        response.sendStatus(400).end();
    }
}


const removeUserFromTeam = async function(userID,teamID){
    // //console.log("leaving team...");
    await global.mysql.query("UPDATE userteams SET isActiveTeam=0 WHERE userID=?;",userID);
    await global.mysql.query("UPDATE users SET teamUpdate=0 WHERE userID=?;",userID);
    await reallocateTeamAdmin(teamID);
    

    // response.sendStatus(200).end();
}


const leaveTeam = async function(request,response){
    await removeUserFromTeam(request.session.userID,request.session.teamID);
    delete request.session.teamID;
    delete request.session.teamAdmin;
    delete request.session.teamAdminName;
    delete request.session.teamName;
    delete request.session.teamPass;
    response.sendStatus(200).end();
}
module.exports.leaveTeam = leaveTeam;


module.exports.swapTeam = async function(request,response,next){
    if(request.body.teamID && request.body.teamPass){
        //console.log("User "+request.session.userName+" swapping teams from "+request.session.teamName+" to "+request.body.teamID);

        await removeUserFromTeam(request.session.userID,request.session.teamID);

        let joinResult = await addUserToTeam(request.session.userID,request.body.teamID,request.body.teamPass);

        if(joinResult!==false){
            request.session.teamID=teamID;
            request.session.teamPass=request.body.teamPass;
            request.session.teamName=joinResult.teamName;
            request.session.teamAdmin=joinResult.teamAdmin;
            request.session.teamAdminName=joinResult.teamAdminName;
            // //console.log(request.session);
            next();
        }
        else{
            response.sendStatus(500).end();
        }


        
    }
    else{
        response.sendStatus(400).end();
    }
}

let initSession = async function(request,userID){
    try{
        if(typeof userID!=="undefined" && userID!==null && !isNaN(Number(userID))){
            let [userData,userDataFields] = await global.mysql.query("SELECT * FROM users LEFT JOIN (SELECT users.userName as teamAdminName,teams.* FROM teams LEFT JOIN users ON teams.teamAdmin = users.userID) as teamdata ON users.userTeam = teamdata.teamID WHERE userID=?;",userID);
            // //console.log(userData);
            return true;
        }
        else{
            return false;
        }
    }
    catch(err){
        //console.log("teamHandler -> initSession -> general");
        //console.log(err);
    }
}
module.exports.initSession = initSession;




const reallocateTeamAdmin = async function(teamID,newAdmin){
    if(typeof teamID!=="undefined"){
        let [teamUsers,teamUsersFields] = await global.mysql.query("SELECT users.userID,lastSeen FROM users LEFT JOIN userteams ON users.userID=userteams.userID WHERE teamID=? AND isActiveTeam=1;",teamID);
        if(typeof newAdmin==="undefined"){
            let [currentAdmin,currentAdminFields] = await global.mysql.query("SELECT teamAdmin FROM teams WHERE teamID=?;",teamID);
            // teamStatus.teamSize=teamUsers.length;
            let onlineMembers=teamUsers.filter(user=>moment(user.lastSeen).isAfter(moment().subtract(10,"minute"))).sort((a,b)=>{return moment(a.lastSeen).isBefore(moment(b.lastSeen)) ? 1 : -1});
            
            if(onlineMembers.length>0){
                if(onlineMembers.findIndex(member=>member.userID===currentAdmin[0].userID)===-1){
                    await global.mysql.query("UPDATE teams SET teamAdmin=? WHERE teamID=?;",[onlineMembers[0].userID,teamID]);
                // global.userCache.
                
                }
            }
        }
        else if(!isNaN(Number(newAdmin))){
            await global.mysql.query("UPDATE teams SET teamAdmin=? WHERE teamID=?;",[newAdmin,teamID]);
            // await global.mysql.query("UPDATE users SET teamUpdate=1 WHERE userTeam=?;",teamID);
        }
        await global.mysql.query("UPDATE users SET teamUpdate=1 WHERE userID IN (?);",teamUsers.map(user=>user.userID));
        await republishTeam(teamID);
    }
}
module.exports.reallocateTeamAdmin = reallocateTeamAdmin;

const forceCaptainChange = async function(request,response){
    let teamID=request.body.teamID;
    try{

        let [teamUsers,teamUsersFields] = await global.mysql.query("SELECT users.userID,lastSeen FROM users LEFT JOIN userteams ON users.userID=userteams.userID WHERE teamID=? AND isActiveTeam=1;",teamID);
        let [currentAdmin,currentAdminFields] = await global.mysql.query("SELECT teamAdmin FROM teams WHERE teamID=?;",teamID);
        let onlineMembers=teamUsers.filter(user=>user.userID!==currentAdmin[0].teamAdmin).sort((a,b)=>{return moment(a.lastSeen).isBefore(moment(b.lastSeen)) ? 1 : -1});
        await reallocateTeamAdmin(teamID,onlineMembers[0].userID);
    
        response.sendStatus(200).end();
    }
    catch(err){
        response.sendStatus(400).end();
    }
    
}


module.exports.forceCaptainChange = forceCaptainChange;

const republishTeam = async function(teamID){
    // let [teamDetails,teamDetailsFields] = await global.mysql.query("SELECT userName,teamID,teamName,teamAdmin,teamPass FROM teams LEFT JOIN users ON teams.teamAdmin=users.userID WHERE teamID=?;",[teamID]);
    //     global.fayeClient.publish("/team/"+teamID,{
    //         ...teamDetails[0],
    //         ext:{serverKey:global.serverKey}
    //     });
    




    let [teamDetail,teamDetailFields] = await global.mysql.query("SELECT teamName,teamDesc,teamPass,teamAdmin,userName as teamAdminName FROM teams LEFT JOIN users ON teams.teamAdmin=users.userID WHERE teamID=?;",[teamID]);
    if(teamDetail.length>0){

        let teamStatus = {teamName:teamDetail[0].teamName,teamDesc:teamDetail[0].teamDesc,teamAdmin:teamDetail[0].teamAdmin,teamAdminName:teamDetail[0].teamAdminName};
        let [teamUsers,teamUsersFields] = await global.mysql.query("SELECT users.userID,userName,lastSeen,quizID FROM users LEFT JOIN userteams on users.userID = userteams.userID WHERE teamID=? AND userteams.isActiveTeam=1;",teamID);
        for(let member of teamUsers){
            member.lastSeen = moment(member.lastSeen).format();
        }
        teamStatus.members=teamUsers;
        
        global.fayeClient.publish("/team/"+teamID,{
            ...teamStatus,
            ext:{serverKey:global.serverKey}
        });
        return true;
        
    }
    else{
        return false;
    }
   
    

}

module.exports.republishTeam = republishTeam;

module.exports.changeTeamAdmin = async function(request,response){
    if(request.body.newAdmin && request.session.userID===request.session.teamAdmin){
        let [newCaptainDetail,newCaptainDetailFields] = await global.mysql.query("SELECT lastSeen FROM users WHERE userID=?;",request.body.newAdmin);
        if(newCaptainDetail.length===1){
            if(moment(newCaptainDetail[0].lastSeen).isAfter(moment().subtract(10,"minutes"))){
                await reallocateTeamAdmin(request.session.teamID,request.body.newAdmin);
                response.sendStatus(200).end();
            }
            else{
                response.sendStatus(400).end();
            }

        }
        else{
            response.sendStatus(400).end();
        }

    }
    else{
        response.sendStatus(400).end();
    }
}




// response.sendStatus(400).end(); // Bad request
// response.sendStatus(404).end(); // Not found