const nunjucks = require('nunjucks');
const fs = require('fs');
const sgMail = require('@sendgrid/mail');

// require('dotenv').config();
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
nunjucks.configure('templates');

async function sendEmailConfirmation(emailAddress, userId, verificationKey) {
    if (!emailAddress || !userId || !verificationKey) {
        throw new Error('missing required params');
    }

    const context = {
        subjectLine: `Validate your UK Quarantine Quiz account!`,
        verificationLink: `https://theukquarantinequiz.co.uk/auth/validate?uid=${userId}&vkey=${encodeURIComponent(verificationKey)}`
    };
    const html = nunjucks.render('email_confirm.njk', context);
    const text = `You've created an account at thequarantinequiz.co.uk using this email address, you just need to activate it!
If it wasn't you, please ignore this email.
    
Click the link below to verify your account
=> ${context.verificationLink}`;

    //console.log(context.verificationLink);
    await sgMail.send({
        to: emailAddress,
        from: "hello@theukquarantinequiz.co.uk",
        subject: context.subjectLine,
        text,
        html
    });
}

async function sendPasswordReset(emailAddress, resetKey) {
    if (!emailAddress || !resetKey) {
        throw new Error('missing required params');
    }

    const context = {
        subjectLine: `Password reset for UK Quarantine Quiz`,
        resetLink: `https://theukquarantinequiz.co.uk/?resetkey=${encodeURIComponent(resetKey)}`
    };
    const html = nunjucks.render('password_reset.njk', context);
    const text = `We've received a request to reset your thequarantinequiz.co.uk account.
If it wasn't you, please ignore this email.
    
Click the link below to change your password
=> ${context.resetLink}`;

    await sgMail.send({
        to: emailAddress,
        from: "passwordreset@theukquarantinequiz.co.uk",
        subject: context.subjectLine,
        text,
        html
    });



    // console.log(process.env.SENDGRID_API_KEY)


    // const msg = {
    //     to: 'test@example.com',
    //     from: 'mattcee233@gmail.com',
    //     subject: 'Sending with Twilio SendGrid is Fun',
    //     text: 'and easy to do anywhere, even with Node.js',
    //     html: '<strong>and easy to do anywhere, even with Node.js</strong>',
    //   };
    //   sgMail.send(msg);
}

module.exports = {
    sendEmailConfirmation,
    sendPasswordReset
};
