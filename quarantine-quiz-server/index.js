const envVars = require('dotenv').config();
const express = require("express");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const server = express();
const session = require("express-session");
const mysqlSession = require("express-mysql-session")(session);
const mysql = require("mysql2");
// const passport = require("passport");
const moment = require("moment");
const http = require("http");
const faye = require("faye");
const fayeServer = new faye.NodeAdapter({mount: '/websocket', timeout: 45,ping:10});
const NodeCache = require("node-cache");

const { AnswerEngine } = require("../answer-engine/answerEngine");
const child = require('child_process');

global.userCache = new NodeCache();
global.serverKey=process.env.serverKey;

const dbConfig = {
    host:process.env.dbHost,
    // port:process.env.dbPort,
    user:process.env.dbUser,
    password:process.env.dbPass,
    database:process.env.dbSchema,
    connectionLimit:10
};

const dbConnection=mysql.createPool(dbConfig);
global.mysql = dbConnection.promise();

// setup helmet
server.use(helmet());

// set up session store
const sessionStore= new mysqlSession({createDatabaseTable:true},global.mysql);
server.use(session({
    key:"sessionCookie",
    secret:"FUtn8kYfthT2imSP1CoZAMMkeJVkGP3skRV6O8Zw9vl9RlVAdUHN7kJIXRO6kApw",
    store:sessionStore,
    resave:false,
    saveUninitialized:false
}));




async function checkLoggedIn(request,response,next){
    if(request.session.loggedIn===true){
        // let userLastSeen = global.userCache.get(request.session.userID);
        // if(typeof userLastSeen==="undefined" || moment(userLastSeen.lastSeen).isBefore(moment().subtract(3,"minutes"))){
        //     global.userCache.set(request.session.userID,{lastSeen:moment().toDate(),teamUpdate:userLastSeen.teamUpdate});
            // await global.mysql.query("UPDATE users SET lastSeen=NOW() WHERE userID=?;",request.session.userID);
            
            // //console.log(teamUpdate);
            let teamUpdate = [];
            let teamUpdateFields = [];
            try{
                [teamUpdate,teamUpdateFields] = await global.mysql.query("SELECT users.quizID,users.teamUpdate,userteams.* FROM users LEFT JOIN userteams ON users.userID=userteams.userID WHERE users.userID=? AND userteams.isActiveTeam=1;",request.session.userID);
                // if(teamUpdate.length!==0){
                //     if(teamUpdate[0].teamUpdate===1){
                        // request.session.quizID=teamUpdate[0].quizID;
                        // request.session.teamID=teamUpdate[0].teamID;
                        let [teamResult,teamResultFields] = await global.mysql.query("SELECT userName,teamID,teamName,teamAdmin,teamPass FROM teams LEFT JOIN users ON teams.teamAdmin=users.userID WHERE teamID=?;",[teamUpdate[0].teamID]);
                        if(teamResult.length===1){
                            request.session.teamAdmin=teamResult[0].teamAdmin;
                            request.session.teamAdminName=teamResult[0].userName;
                            request.session.teamName=teamResult[0].teamName;
                        }
                        
                        // await global.mysql.query("UPDATE users SET teamUpdate=0 WHERE userID=?;",request.session.userID);
                //     }
                // }
            }   
            catch(err){

                console.log(err);
                console.log(teamUpdate);
            }
        // }
        next();
    }
    else{
        response.sendStatus(401).end();
    }
}




global.pingUpdate=[];

async function ping(request,response){
    try{
        global.pingUpdate.push(request.session.userID);

    }
    catch(err){
        console.log(err);
    }
    response.sendStatus(200).end();
}


global.pingUpdater = setInterval(async ()=>{
    if(global.pingUpdate.length>0){
        try{
            console.log(process.memoryUsage());
            let filterUpdate=global.pingUpdate.filter((q,i,pu)=>{return pu.indexOf(q)===i});
            global.pingUpdate=[];
            console.log("ping updating db user fields for "+filterUpdate.length + " users");
            // console.log(filterUpdate);
            await global.mysql.query("UPDATE users SET lastSeen=? WHERE userID IN (?);",[moment().toDate(),filterUpdate]);
        }
        catch(err){
            console.log(err);
        }    
        
    }
},90000);

global.adminKeysList = [];

async function getAdminList(){
    let [adminList,adminListFiends] = await global.mysql.query("SELECT * FROM users LEFT JOIN userteams ON users.userID=userteams.userID WHERE isAdmin=1 AND isActiveTeam=1;");
    global.adminKeysList = adminList.map(user=>{return {userID:user.userID,userName:user.userName,defaultTeam:user.teamID}});
}

getAdminList();

async function checkAdmin(request,response,next){
    await getAdminList();
    if(global.adminKeysList.findIndex(a=>a.userID===request.session.userID)!==-1){
        // //console.log("admin request...");
        // //console.log(request);
        next();
    }
    else{
        response.sendStatus(401).end();
    }
}


function nocache(req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next();
}


// basic page route
server.use(express.static("../build"));
server.use("/img",express.static("../img"));


server.use(bodyParser.json());
const authHandler=require("./authHandler");
const teamHandler=require("./teamHandler");
const quizApi=require("./quizApi");

server.get("/api/getvidlink",quizApi.getVidLink);

// authentication handling (account setup)
server.post("/auth/register",authHandler.register);

server.post("/auth/forgot",authHandler.forgottenPass);
server.post("/auth/reset",authHandler.changePass);
server.get("/auth/validate",authHandler.validateEmail);

// don't cache any of the below API query responses.
server.use(nocache);

server.post("/auth/login",authHandler.login,checkLoggedIn,authHandler.query);

server.get("/auth/query",checkLoggedIn,authHandler.query);
server.get("/auth/logout",checkLoggedIn,authHandler.logout);
server.get("/api/ping",checkLoggedIn,ping);

// team management
server.post("/api/createteam",checkLoggedIn,teamHandler.createTeam,authHandler.query);
server.post("/api/jointeam",checkLoggedIn,teamHandler.joinTeam,authHandler.query);
server.get("/api/getteam",checkLoggedIn,teamHandler.getTeam);
server.post("/api/setcaptain",checkLoggedIn,teamHandler.changeTeamAdmin);
server.post("/api/teamname",checkLoggedIn,teamHandler.changeName);
// server.get("/api/leaveteam",checkLoggedIn,teamHandler.leaveTeam);
server.post("/api/swapteam",checkLoggedIn,teamHandler.swapTeam,authHandler.query);
server.get("/api/teamlist",checkLoggedIn,teamHandler.sendTeamList);

// incoming data routes
// server.post("/api/vote",checkLoggedIn);
server.get("/api/getquizzes",quizApi.getQuizzes);
server.post("/api/suggest",checkLoggedIn,quizApi.suggest);
server.post("/api/answer",checkLoggedIn,quizApi.answer);
server.post("/api/chat",checkLoggedIn,quizApi.chatter);
server.get("/api/switchquiz",checkLoggedIn,quizApi.switchQuiz);


// admin functionality
server.post("/api/newQuestion",checkAdmin,quizApi.newQuestion);
server.post("/api/newQuiz",checkAdmin,quizApi.newQuiz);
server.post("/api/set",checkAdmin,quizApi.setQuestion);
server.post("/api/upload",checkAdmin,quizApi.saveFile);
server.get("/api/send",checkAdmin,quizApi.sendQuestion);
server.get("/api/force",checkAdmin,quizApi.forceCorrect);
server.get("/api/remark",checkAdmin,quizApi.forceRemark);
server.get("/api/getquiz",checkAdmin,quizApi.getQuiz);
server.post("/api/setvidlink",checkAdmin,quizApi.setVidLink);
server.get("/api/leaderboard",checkAdmin,quizApi.leaderboard);
server.get("/api/sendleaderboard",checkAdmin,quizApi.sendLeaderBoard);
server.post("/api/clearleaderboard",checkAdmin,quizApi.clearLeaderboard);
server.post("/api/changecaptain",checkAdmin,teamHandler.forceCaptainChange);
// attach faye server to the express server

// server.use(checkAdmin);
server.use("/host",checkAdmin,express.static("../host/build"));
// start the server
let httpServer = http.createServer(server);

var authorized = async function(message) {
    // //console.log(message);
    try{
        if(!["/question","/vid","/leaderboard"].some(chan=>message.subscription.includes(chan))){
            let teamID = message.subscription.slice(message.subscription.lastIndexOf("/")+1);
            if(teamID==="*"||teamID==="answers"){
                let [adminKey,adminKeyFields] = await global.mysql.query("SELECT * FROM users WHERE isAdmin=1;");
                let adminRecord = adminKey.find(admin=>admin.userID===message.ext.userID && admin.userKey===message.ext.userKey);
                if(typeof adminRecord!=="undefined"){
                    // //console.log("admin level listener enabled for channel "+message.subscription);
                    return true;

                }
                else{
                    // //console.log("fail A");
                    return false;
                }
            }
            else{
                teamID=parseInt(teamID);
            }

            // //console.log(teamID);
            
            // //console.log(message.ext);
            let userID=message.ext.userID;
            let userKey=message.ext.userKey;
            let [userData,userFields] = await global.mysql.query("SELECT userteams.teamID FROM users LEFT JOIN userteams ON users.userID=userteams.userID WHERE users.userID=? AND users.userKey=? AND userteams.isActiveTeam=1; ",[userID,userKey]);
            // //console.log(userData);
            if(userData.length>0){
                // //console.log(userData[0])
                if(userData.findIndex(team=>{return team.teamID===teamID})!==-1){

                    return true;
                }
                else{
                    // //console.log("fail B");
                    return false;
                }
            }
            else{
                // //console.log("fail C");
                return false;
            }
        }
        else{
            return true;
        }
    }
    catch(err){
        //console.log("----------==========UNAUTHORISED FAYE CONNECTION ATTEMPT!!!=======---------");
        //console.log(message);
        //console.log(err);
        //console.log("-----------------------------------------------------------------------");
        return false;
    }

  };

fayeServer.addExtension({
    incoming: async function(message, callback) {
        // //console.log(message);
        if (!message.channel.match(/^\/meta\//)) {

            // console.log(message.data);
            try{
                if(message.data.ext.serverKey===global.serverKey){
                    delete message.data.ext;
                }
                else{
                    //console.log("rejected internal message, something wrong with server keys?");
                    message.error="401::Not so fast!"
                }
            }
            catch(err){
                console.log("this broke!");
                console.log(err);
                console.log(message.data)
            }
            // //console.log(message);
        }

        if (message.channel === '/meta/subscribe') {
            // if (subscriptions.indexOf(message.subscription) >= 0) {
            let auth = await authorized(message);
              if (!auth){

                // //console.log(message.ext.userID + " Blocked from "+ message.subscription);
                message.error = '401::Authentication required';
              }
              else{
                // //console.log(message.ext.userID + " is now subscribed to "+ message.subscription);
              }
            // }

          }
        
        callback(message);
      },

      outgoing: function(message, callback) {
        // //console.log(message);
        // if (message.ext){
        //     delete message.ext;
        // }
        // if (!message.channel.match(/^\/meta\//)) {
        //     //console.log("sending...")
        //     //console.log(message);
        // }
        callback(message);
      }
  });


fayeServer.attach(httpServer);
httpServer.listen(Number(process.env.serverPort),()=>{console.log("Quarantine Quiz Server ONLINE!")});

// global.fayeClient = new faye.Client("http://localhost:"+process.env.serverPort+"/websocket");
global.fayeClient = fayeServer.getClient();
if(process.env.fayeListener==="true"){
    global.fayeClient.addExtension(
        {
            incoming: function(message, callback) {
                // //console.log(message);


                callback(message);
                },

            outgoing: function(message, callback) {
                // message.data.ext={};
                // message.data.ext.serverKey=global.serverKey;

                if (!message.channel.match(/^\/meta\//)) {
                    //console.log(message);
                }
                callback(message);
            }
        }
    );
}

global.fayeClient.connect();

if(process.env.internalAnswerEngine==="true"){
    const answerEngine = new AnswerEngine(global.mysql, global.fayeClient, global.serverKey);
    answerEngine.checkAnswers().then(() => {
        //console.log(`initial check complete`);
    });
} else {
    // const answerEngine = child.fork('../answer-engine/answerEngine.js');
    console.log(`this is where i'd spin up the answer engine, but Jish turned it off.`)
}



// quizApi.fakeQuestions(20000);
