import React from 'react';
import 'antd/dist/antd.css';
import {Layout,Typography,Form, Input, Button, Checkbox,Row,Col,Modal,Affix,message,Card,Tag,Icon,notification,Drawer,Table,Alert,Divider,Popconfirm,Radio,Tabs, Steps} from 'antd';
// import { Icon type="UserOutlined", Icon type="LockOutlined", Icon type="MailOutlined" } from '@ant-design/icons';
import Image from 'react-graceful-image';
import faye from 'faye';
import moment from 'moment';
import reactGA from 'react-ga';
import {BrowserView, MobileView, isBrowser, isMobile} from 'react-device-detect';
import {
  EmailShareButton,
  FacebookShareButton,
  InstapaperShareButton,
  // LineShareButton,
  LinkedinShareButton,
  // LivejournalShareButton,
  // MailruShareButton,
  // OKShareButton,
  // PinterestShareButton,
  // PocketShareButton,
  RedditShareButton,
  TelegramShareButton,
  TumblrShareButton,
  TwitterShareButton,
  // ViberShareButton,
  // VKShareButton,
  WhatsappShareButton,
  // WorkplaceShareButton,
  EmailIcon,
  FacebookIcon,
  InstapaperIcon,
  LineIcon,
  LinkedinIcon,
  LivejournalIcon,
  MailruIcon,
  OKIcon,
  PinterestIcon,
  PocketIcon,
  RedditIcon,
  TelegramIcon,
  TumblrIcon,
  TwitterIcon,
  ViberIcon,
  VKIcon,
  WeiboIcon,
  WhatsappIcon,
  WorkplaceIcon,
} from "react-share";
import headerImage from './pics/LargeHeader.png';
import vidBanner from './pics/8pmBanner.png';
import ClipboardJS from 'clipboard';
import { TIS620_THAI_CI } from 'mysql2/lib/constants/charsets';


let clipBoard=new ClipboardJS("#copybit");
const {Title,Text}=Typography;
const { Header, Content, Sider } = Layout;

let cYellow="#FFE92B";
let cBlue="#113B7A";
let cDarkBlue="#0E2B56";

reactGA.initialize("UA-161000564-1");

const getUrlVar = function getUrlVar(varName,url) {
    if(typeof url==="undefined"){
        url=window.location.href;
    }
    var vars = {};
    var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars[varName];
}
  
// module.exports.getUrlVar=getUrlVar;
let fayeClient = new faye.Client("./websocket");
// let fayeClient = new faye.Client("http://localhost:30001/websocket");
let userIDMain,userKeyMain;


let fayeExtension = {
  outgoing: function(message, callback) {
    // if(message.channel==="/meta/subscribe")
    message.ext = message.ext || {};
    message.ext.userID = userIDMain;
    message.ext.userKey = userKeyMain;
    //console.log(message);
    callback(message);
  },
  incoming: function(message,callback){
    //console.log(message);
    callback(message);
  }
}



async function setFaye(uid,ukey){
  //console.log("setting up faye extension - values "+uid+" - "+ukey)
  userIDMain=uid;
  userKeyMain=ukey;
    // fayeClient = fayeClientParam;
    try{
      fayeClient.removeExtension(fayeExtension)
    }
    catch(err){
      //console.log(err);
    }
    fayeClient.addExtension(fayeExtension);
    await (async () => new Promise(resolve => setTimeout(resolve, 200)))();
    return true;
}







export class Top extends React.Component {
    constructor(props){
      super(props);
      this.state={
        loggedIn:false,
        setup:{
          headerUrl:"./img/ukqqTitle.png",
          logoUrl:"./img/ukqqLogo.png",
          coverUrl:"./img/ukqqBanner.png"
        }
      }
      
      this.stateSetter = this.setState.bind(this);
      this.logOut=this.logOut.bind(this);
      
    }
  
    async componentDidMount(){
  
      // DNS propagation issues, redirect to IP
      // window.location.replace("http://app.theukquarantinequiz.co.uk/");
  
      if(getUrlVar("validate")==="done"){
        message.success("Email Address Validated Successfully!");
      }

      
      let response = await fetch("./auth/query",{credentials:"same-origin"});
      if(response.ok){
        let data = await response.json();
        //console.log("Session Query Completed...")
        //console.log(data);
        let userID=data.userID;
        let userKey=data.userKey;
        await setFaye(userID,userKey);
        if(!data.teamID){
          data.showTeamJoiner=true;
        }
        reactGA.set({'user_id': data.userID}); // Set the user ID using signed-in user_id.
        data.quizStatus=data.quizStatus.filter(q=>q!==null);
        try{
          data.setup=data.quizDetail[0];
          delete data.quizDetail;
        }
        catch(err){}
        this.setState({...data,loggedIn:true});
        
      }
      
    }

    componentDidUpdate(prevProps,prevState){
      if(prevState.userID!==this.state.userID || prevState.teamID!==this.state.teamID){
        setFaye(this.state.userID,this.state.userKey);
        // let that=this;
        this.pingTimer=setInterval(async ()=>{await fetch("./api/ping",{method:"GET",credentials:"same-origin"})},60000)
      }
    }
  
    async logOut(){
      //console.log("trying to log out");
      let response = await fetch("./auth/logout",{credentials:"same-origin"});
      if(response.ok){
        //console.log("success?");
        document.location.replace("https://theukquarantinequiz.co.uk");
      }
    }
  
    
  
    
  
    render(){
      let returnValue;
      if(isBrowser){

        returnValue = (
          <div className="App">
            <Layout>
              
              <Sider width={560} className={"sider"} style={{color:"#FFFFFF",height:"calc(100vh-40px)"}}>
                  <VideoPlayer {...this.state} stateSetter={this.stateSetter}/>
                  <ChatBox {...this.state} />
              </Sider>
                
              
                
                
                
    
              
              <Content>
                
                <div style={{display:"flex",flexFlow:"column",height:"100vh",backgroundColor:cBlue,flex:"0 1 auto"}}>
                  {/* <Header className={"header"} style={{backgroundColor:cBlue,flex:"0 1 auto"}}> */}
                    <Row type="flex" justify="space-around" align="middle" style={{height:64,flex:"0 0 auto"}}>
                      <Col span={16} >
                        {/* <Title level={2} style={{color:"#FFFFFF", lineHeight:"64px",margin:0}}>
                          The UK Quarantine Quiz
                        </Title> */}
                        <img src={this.state.setup.headerUrl} style={{height:"45px"}} alt={"The UK Quarantine Quiz"}/>
                      </Col>
                      <Col span={8} style={{align:"right"}}>
                        {this.state.loggedIn ? <><Text style={{color:cYellow}} strong>{this.state.userName}</Text><Text style={{color:cYellow}}> in team </Text><Text  style={{color:cYellow}} strong>{this.state.teamName}</Text><Text  style={{color:cYellow}}>#{this.state.teamID} <Button style={{marginLeft:"10px",color:cBlue,backgroundColor:cYellow,borderRadius:0,border:0,fontWeight:"bold"}} onClick={()=>{this.logOut()}} >Logout</Button></Text><QuizSwitcher {...this.state}/></> : <LogIn appSetState={this.stateSetter} {...this.state}/>}
                        
                      </Col>
                    </Row>
                  {/* </Header> */}
                  <TeamJoiner {...this.state} appSetState={this.stateSetter}/>
                  


                  {/* <Content> */}
                    <TeamBar appSetState={this.stateSetter} {...this.state}/>
                    <Quiz appSetState={this.stateSetter} {...this.state} isCaptain={this.state.teamAdmin===this.state.userID}/>
                  {/* </Content> */}
                </div>
              </Content>
            </Layout>
          </div>
        );

      }

      else{
        returnValue = (
          <div className="App">
            <Layout>
              
                
                
                <div style={{padding:"10px"}}>
                  <Row align={"middle"}>
  
                    <Col span={24} >
                      <Title level={2} style={{color:"#000000"}}>
                        The UK Quarantine Quiz
                      </Title>
                    </Col>
                  </Row>
                  <Row align={"middle"}>
                    <Col span={24} style={{textAlign:"center"}}>
                      {this.state.loggedIn ? <><Typography.Text style={{color:"#000000"}}>{this.state.userName} in team {this.state.teamName}#{this.state.teamID} <Button style={{marginLeft:"10px",color:"#0000FF"}} onClick={()=>{this.logOut()}}>Logout</Button></Typography.Text><QuizSwitcher {...this.state}/></> : <LogIn appSetState={this.stateSetter} {...this.state}/>}
                      
                    </Col>
                  </Row>
                </div>
              
              <TeamJoiner {...this.state} appSetState={this.stateSetter}/>
    
              
              <Layout>
                <VideoPlayer {...this.state} stateSetter={this.stateSetter}/>
                <ChatBox {...this.state} />
                <TeamBar appSetState={this.stateSetter} {...this.state}/>
                <Quiz appSetState={this.stateSetter} {...this.state} isCaptain={this.state.teamAdmin===this.state.userID}/>
                
              </Layout>
            </Layout>
          </div>
        );
      }



      return returnValue;
    }
  }

export class QuizSwitcher extends React.PureComponent{
  constructor(props){
    super(props);
    this.state={
      quizzes:[],
      active:false
    };
    this.swapQuiz=this.swapQuiz.bind(this);
    this.getQuizzes=this.getQuizzes.bind(this);
    this.activate=this.activate.bind(this);
  }

  activate(){
    if(typeof this.props.quizID!=="undefined"){
      this.setState({active:!this.state.active});
    }
  }

  async swapQuiz(quizToGoTo){
    // let quizToGoTo;
    // if(this.props.quizID===1){
    //   quizToGoTo=2;
    // }
    // else{
    //   quizToGoTo=1;
    // }

    let response = await fetch("./api/switchquiz?quizID="+quizToGoTo,
      {
        method:"GET",
        credentials:"same-origin",
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
    if(response.ok||response.redirected){

      let joinString = getUrlVar("q");
      let newLocation = document.location.href;
      console.log(newLocation);
      if(typeof joinString!=="undefined"){
        console.log("chopping q");
        newLocation = newLocation.replace("q="+joinString,"");
      }
      console.log(newLocation);
      
      document.location.href=newLocation;
    }
    else{
      message.error("failed to switch quiz...");
    }
  }

  async getQuizzes(){
    let response = await fetch("./api/getquizzes",
      {
        method:"GET",
        credentials:"same-origin",
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
    if(response.ok){
      let data = await response.json();
      //console.log(data);
      let joinString = getUrlVar("q");
      
      if(typeof joinString!=="undefined"){
        //console.log(joinString);
        let quiz = data.find(quiz=>joinString.toLowerCase().includes(quiz.joinCode.toLowerCase())).quizID;
        //console.log(quiz);
        if(typeof quiz!=="undefined"){
          await this.swapQuiz(quiz);
        }
        
      }
      this.setState({quizzes:data});
    }
    else{
      message.error("failed to get quiz list...");
    }
  }

  componentDidMount(){
    this.getQuizzes();
  }

  render(){
    
    // return(

      // <Popconfirm title={this.props.quizID===2?"This will return you to the public quiz, do you want to do that?":"This will take you to the RTBI Quiz, are you sure you want to continue?"} onConfirm={this.swapQuiz}>
      //   {/* <Button style={{marginLeft:"10px",color:"#0000FF"}} >{this.props.quizID===1?"Private Quiz":"Public Quiz"}</Button> */}
      //   <img style={{marginLeft:"10px",cursor:"pointer"}} height="40px" src={this.props.quizID!==2?"./img/rtbi.gif":"./img/LogoSmall.png"} alt={this.props.quizID!==2?"Join the RTBI quiz!":"Go back to the UKQQ quiz!"}/>
        
      // </Popconfirm>
      
    // );
    let returnValue=null;
    let modalValue=null;
    if(this.state.quizzes.length>0 && this.state.active===true){
      
      
      modalValue=<Modal centered onCancel={()=>{this.activate()}} visible width={"50%"} footer={null} header={null}>
          <Row type="flex" justify="center" align="middle">
            {this.state.quizzes.map(quiz=>{return <>
              <Col span={isMobile?24:6} style={{textAlign:"center"}}>
                <img src={quiz.logoUrl} width={100} height={100} alt={quiz.nameText+" Quiz Logo"}/>
                <br/>
                <h2>{quiz.nameText}</h2>
                <br/>
                {/* Players online count 
                <br/> */}
                <Button onClick={()=>{this.swapQuiz(quiz.quizID)}} disabled={quiz.quizID===this.props.quizID} type={quiz.quizID===this.props.quizID?"primary":"ghost"} >{quiz.quizID===this.props.quizID?"Current Quiz":"Join Quiz"}</Button>
              </Col>
            </>})}
          </Row>
        </Modal>
      
    }
    returnValue=<Button onClick={()=>{this.activate()}} style={{marginLeft:"10px",color:cBlue,backgroundColor:cYellow,borderRadius:0,border:0,fontWeight:"bold",position:"relative",top:"-2px"}}>Swap Quiz <img style={{marginLeft:"10px",cursor:"pointer"}} height="32px" src={this.props.setup.logoUrl} alt={"Swap to another quiz!"}/>{modalValue}</Button>;


    return returnValue;
  }
}



export class LogIn extends React.PureComponent{
    constructor(props){
      super(props);
      this.state={
        failed:false,
        registering:false,
        resetKey:getUrlVar("resetkey"),
        
      }
      this.logIn=this.logIn.bind(this);
      // this.reset=this.reset.bind(this);
      this.forgot=this.forgot.bind(this);
      this.storeFormData=this.storeFormData.bind(this);
      this.toggleRegistration=this.toggleRegistration.bind(this);
      this.loginWith=this.loginWith.bind(this);
      
      
      
    }
  
    
    loginWith(user,pass){
      //console.log("trying to log in with user+pass provided...");
      // //console.log(user);
      // //console.log(pass);
      let that=this;
      this.setState({username:user,password:pass},()=>{
        //console.log("state set, attempting login...");
        that.logIn();
      });
    }
  
    async logIn(e){
      try{
        e.preventDefault(); // if triggered by an event just stop it doing anything, if not triggered by an event this throws an error, catch and throw away
      }
      catch(err){}
      // values.persist();
      // //console.log("sending...")
      // //console.log(values);
      if(this.state.logInInProgress!==true){
        this.setState({logInFailed:false,logInInProgress:true});
        let response = await fetch("./auth/login",{
          method:"POST",
          credentials:"same-origin",
          headers: {
            'Content-Type': 'application/json'
          },
          body:JSON.stringify({
            user:this.state.username,
            pass:this.state.password
          })
        });
        if(response.ok){
          let data=await response.json();
          //console.log(data);
          let userID=data.userID;
          let userKey=data.userKey;
          await setFaye(userID,userKey);
          
          if(data.teamID===null){
            data.showTeamJoiner=true;
          }
          reactGA.set({'user_id': data.userID}); // Set the user ID using signed-in user_id.
          //console.log(data);
          try{
            data.setup=data.quizDetail[0];
            delete data.quizDetail;
          }
          catch(err){}
          this.props.appSetState({...data,logInInProgress:false,loggedIn:true});
          this.setState({registering:false});
        }
        else{
          
          this.setState({logInFailed:true,logInInProgress:false})
        }
      }
    }
  
    
  
    async forgot(e){
      try{
        e.preventDefault();
      }
      catch(err){}
      
      if(!this.state.username){
        message.error("Enter your username, then click forgotten password to get a reset email");
      }
      else{
        if(this.state.resetInProgress!==true){
          this.setState({resetInProgress:true});
  
        
          
          let response = await fetch("./auth/forgot",{
            method:"POST",
            credentials:"same-origin",
            headers: {
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({
              userName:this.state.username
            })
          });

          if(response.ok){
            message.success("Password reset email sent, be sure to check your spam!");
            this.setState({resetInProgress:false});
          }
          else{
            message.error("Can't seem to find that username...");
            this.setState({resetInProgress:false});
          }
        }
      }
    }
  
    storeFormData(change){
      // change.persist();
      // change.preventDefault();
      // //console.log(change);
      // //console.log(this.loginFormRef.current);
      // // let updateObject = {}
      // // for(let field of allVals){
      // //   updateObject[field.name[0]]=field.value;
      // // }
      let update={};
      update[change.target.name]=change.target.value;
      // //console.log(update);
      this.setState({...update,regUsed:false,regFailed:false});
    }
  
    toggleRegistration(){
      this.setState({registering:!this.state.registering})
    }
  
   
    render(){
      
        let returnValue;


        if(typeof this.state.resetKey!=="undefined"){
            // resetting, show reset form
            returnValue = <PassReset resetKey={this.state.resetKey} resetLogin={this.loginWith} />;

        }

        else if(this.state.registering){
            // registering, show registration form instead!
            returnValue = <Register loginWith={this.loginWith} />
        }


        else {
            returnValue=(
                    <Form name="login" onSubmit={this.logIn} onChange={this.storeFormData}>

                        <Form.Item
                            name="username"
                            rules={[{ required: true, message: 'Please input your username/Email!' }]}
                        >
                            <Input 
                                prefix={<Icon type="user" className="site-form-item-icon" />} 
                                placeholder="Username/Email" 
                                name="username" 
                            />
                        </Form.Item>

                        <Form.Item
                            name="password"
                            rules={[{ required: true, message: 'Please input your password!' }]}
                        >
                            <Input
                                prefix={<Icon type="lock" className="site-form-item-icon" />}
                                name="password"
                                type="password"
                                placeholder="Password"
                            />
                        </Form.Item>

                        <a onClick={this.forgot}>Forgotten Password</a>

                        <Form.Item shouldUpdate={true}>
                            
                            <Button
                                type="primary"
                                htmlType="submit"
                                disabled={this.state.logInInProgress}
                            >
                                Log in
                            </Button>
                            
                                                    
                            <Button
                                type="default"
                                htmlType="button"
                                onClick={this.toggleRegistration}
                            >
                                Register
                            </Button>

                            {this.state.logInFailed?
                                <Alert message="Looks like your login or password is incorrect, try again or reset your password." type="error" showIcon />
                            :null}

                        </Form.Item>
                    
                    </Form>
            );  
                
             
            if(isBrowser){
                // encase form in a Modal if we are in browser
                returnValue = (
                    
                    <Modal footer={null} closable={false} visible={true}>
                        {returnValue}
                    </Modal>

                );
            }
        }
        
        return returnValue;
    }
  }

export class PassReset extends React.PureComponent{
    constructor(props){
      super(props);
      this.state={
        // failed:false,
        // registering:false,
        // resetKey:getUrlVar("resetkey"),
      }
      this.reset=this.reset.bind(this);
      this.storeFormData=this.storeFormData.bind(this);
    }
  
    
  
  
    
  
    async reset(e){
      try{
        e.preventDefault(); // if triggered by an event just stop it doing anything, if not triggered by an event this throws an error, catch and throw away
      }
      catch(err){}
      //console.log("resetting...");
      if(this.state.newpass!==this.state.newpass2){
        this.setState({passwordMismatch:true});
      }
      else{
        if(this.state.resetInProgress!==true){
          this.setState({passwordMismatch:false,resetInProgress:true});
  
        
          //console.log(this.state);
          let response = await fetch("./auth/reset",{
            method:"POST",
            credentials:"same-origin",
            headers: {
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({
              userKey:this.props.resetKey,
              pass:this.state.newpass,
              // email:this.state.remail
            })
          });
          if(response.ok){
            let data=await response.json();
            //console.log(data);
            this.props.resetLogin(data.userName,this.state.newpass);
            this.setState({resetInProgress:false});
          }
          else{
              message.error("Something went wrong resetting your password...");
          }
        }
      }
    }
  
   
    storeFormData(change){
    
      let update={};
      update[change.target.name]=change.target.value;
      
      this.setState({...update});

    }
  
   
    render(){
      
        let returnValue = (
            <Form  name="reset" layout="vertical" onSubmit={this.reset} onChange={this.storeFormData}>
                    
                <Form.Item
                    name="newpass"
                    rules={[{ required: true, message: 'Please input a new password!' }]}
                >
                    <Input
                        prefix={<Icon type="LockOutlined" className="site-form-item-icon" />}
                        type="password"
                        name="newpass"
                        placeholder="New Password"
                    />
                </Form.Item>


                <Form.Item 
                    shouldUpdate={true}
                    name="newpass2"
                    rules={[{ required: true, message: 'Please confirm your password!' }]}
                >
                    <Input
                        prefix={<Icon type="LockOutlined" className="site-form-item-icon" />}
                        type="password"
                        name="newpass2"
                        placeholder="Confirm New Password"
                    />
                </Form.Item>
                

                <Form.Item shouldUpdate={true}>
                    
                    <Button
                        type="primary"
                        htmlType="submit"
                        disabled={this.state.registerInProgress}
                    >
                    Reset Password
                    </Button>
                
                </Form.Item>


                {this.state.passwordMismatch===true ? <Alert type="error" message="Passwords Don't Match, Try Again..." showIcon/> : null}
            </Form>
  
        );


        if(isBrowser){
            returnValue = (

                <Modal visible={true} onOk={this.reset} closable={false} footer={false}>

                    {returnValue}
                    
               </Modal>
                
            ); 
        }
        
      
  
      return returnValue;
      
    }
  }

export class Register extends React.PureComponent{
    constructor(props){
      super(props);
      this.state={
        failed:false,
        
      }
      this.register=this.register.bind(this);
      this.storeFormData=this.storeFormData.bind(this);
      this.showUserAgreement=this.showUserAgreement.bind(this);
      
    }
  
    
  
  
   
  
    async register(values){
      try{
      values.preventDefault();
      }
      catch(err){}
      // //console.log(values);
      if(this.state.rpassword!==this.state.rpassword2){
        this.setState({passwordMismatch:true});
      }
      else if(this.state.ragreement===true){
        if(this.state.registerInProgress!==true){
          this.setState({passwordMismatch:false,registerInProgress:true});
  
        
          //console.log(this.state);
          let response = await fetch("./auth/register",{
            method:"POST",
            credentials:"same-origin",
            headers: {
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({
              user:this.state.rusername,
              pass:this.state.rpassword,
              email:this.state.remail
            })
          });
          if(response.ok){
            this.props.loginWith(this.state.rusername,this.state.rpassword);
          }
          else{
            if(response.status===501){
              this.setState({regUsed:true,registerInProgress:false});
            }
            else{
              this.setState({regFailed:true,registerInProgress:false})
            }
          }
        }
      }
      else{
          message.error("Can't register without agreeing to terms...");
      }
    }
  
  
    storeFormData(change){
      // change.persist();
      // change.preventDefault();
      // //console.log(change);
      // //console.log(this.loginFormRef.current);
      // // let updateObject = {}
      // // for(let field of allVals){
      // //   updateObject[field.name[0]]=field.value;
      // // }
      let update={};
      if(change.target.name==="ragreement"){
        update[change.target.name]=change.target.checked;
      }
      else{
        update[change.target.name]=change.target.value;
      }
      // //console.log(update);
      this.setState({...update,regUsed:false,regFailed:false});
    }
 
    showUserAgreement(){
      Modal.info({
        title:"The UK Quarantine Quiz User Agreement",
        content:(
          <>
          Rules are simple! Don't be abusive to the hosts or anyone else using the platform, be nice, enjoy yourself, and you're also accepting us using cookies to enable the site to work and track our audience. We may use your email from time to time to tell you when we have event's going on or for changes to the platform which we want to announce. By agreeing you are confirming you are happy with all of this.
          </>
        ),
        onOk(){}
      });
    }
  
    render(){
      

        let returnValue;

        returnValue = (
            <Form  name="register" layout="vertical" onSubmit={this.register} onChange={this.storeFormData}>
              <Form.Item
                name="rusername"
                rules={[{ required: true, message: 'Please input your username!' }]}
              >
                <Input prefix={<Icon type="user" className="site-form-item-icon" />} placeholder="Username" name="rusername" />
              </Form.Item>

              <Form.Item
                name="rpassword"
                rules={[{ required: true, message: 'Please input your password!' }]}
              >
                <Input
                  prefix={<Icon type="lock" className="site-form-item-icon" />}
                  type="password"
                  name="rpassword"
                  placeholder="Password"
                />
              </Form.Item>

              <Form.Item 
                shouldUpdate={true}
                name="rpassword2"
                rules={[{ required: true, message: 'Please confirm your password!' }]}
              >
                <Input
                  prefix={<Icon type="lock" className="site-form-item-icon" />}
                  type="password"
                  name="rpassword2"
                  placeholder="Confirm Password"
                />
              </Form.Item>

              <Form.Item
                name="remail"
                
                rules={[
                  {
                    type: 'email',
                    message: 'The input is not valid E-mail!',
                  },
                  {
                    required: true,
                    message: 'Please input your E-mail!',
                  },
                ]}
              >
                <Input 
                  prefix={<Icon type="mail" className="site-form-item-icon" />}
                  name="remail"
                  placeholder="E-Mail Address"
                />
              </Form.Item>
  
              <Form.Item name="ragreement">
                <Checkbox defaultChecked={false} name="ragreement">
                  I have read and agree to the <a onClick={this.showUserAgreement}>user agreement</a>
                </Checkbox>
              </Form.Item>
  
              <Form.Item >
                <Button
                    type="primary"
                    htmlType="submit"
                    
                >
                    Register
                </Button>
              </Form.Item>
              
              {this.state.passwordMismatch===true ? <Alert type="error" message="Passwords Don't Match, Try Again..." showIcon /> : null}
              {this.state.regUsed===true ? <Alert type="error" message="Username/Email already registered..." showIcon /> : null}
              {this.state.regFailed===true ? <Alert type="error" message="Unable to register, server error..." showIcon /> : null}
            </Form>
            
        );


        if(isBrowser){
            returnValue = (
                <Modal visible={true} footer={null} closable={false}>
                    {returnValue}
                </Modal>
            );
        }

        return returnValue;
      
    }
  }

export class TeamJoiner extends React.Component{
    constructor(props){
      super(props);
      this.state={
        mode:"select",
        serverComms:false,
        teamID:"",
        teamPass:""
      }
      this.setState=this.setState.bind(this);
      this.createTeam=this.createTeam.bind(this);
      this.joinTeam=this.joinTeam.bind(this);
      this.recordTeamDetails=this.recordTeamDetails.bind(this);
      this.getCurrentTeams=this.getCurrentTeams.bind(this);
      this.swapTeam = this.swapTeam.bind(this);
    }
  
    
  
    async createTeam(e){
      try{
        e.preventDefault();
      }
      catch(err){}
      // await this.formRef.current.validateFields();
      this.setState({serverComms:true});
      
      let response = await fetch("./api/createteam",{
        method:"POST",
        credentials:"same-origin",
        headers: {
          'Content-Type': 'application/json'
        },
        body:JSON.stringify({
          teamName:this.state.teamName,
          // teamPass:this.state.teamPass,
          teamDesc:this.state.teamDesc
        })
      });
      if(response.ok){
        let data=await response.json();
        //console.log(data);
        document.location.replace("https://theukquarantinequiz.co.uk");
        
        // this.props.appSetState({...data,showTeamJoiner:false});
        // this.setState({mode:"select",serverComms:false});
      }
      else{
        this.setState({serverComms:false});
        message.error("Couldnt Create Team... Are you still online?");
      }
      
      
    }
  
    async joinTeam(e){
      try{
        e.preventDefault();
      }
      catch(err){}
      // await this.formRef.current.validateFields();
      this.setState({serverComms:true});
      
      let response = await fetch("./api/jointeam",{
        method:"POST",
        credentials:"same-origin",
        headers: {
          'Content-Type': 'application/json'
        },
        body:JSON.stringify({
          teamID:this.state.teamID,
          teamPass:this.state.teamPass
        })
      });
      if(response.ok){
        let data=await response.json();
        //console.log(data);
        document.location.replace("https://theukquarantinequiz.co.uk");
        
        // this.props.appSetState({...data,showTeamJoiner:false});
        // this.setState({mode:"select",serverComms:false});

      }
      else{
        this.setState({serverComms:false});
        if(response.status===409){
          message.error("That team number doesnt exist or the passcode is wrong... try again");
        }
        else{
          message.error("Couldnt Join Team... Are you still online?");
        }
        
      }
  
    }



    async getCurrentTeams(){
      let response = await fetch("./api/teamlist",{
        method:"GET",
        credentials:"same-origin",
        headers: {
          'Content-Type': 'application/json'
        }
      });
      if(response.ok){
        let data=await response.json();
        //console.log(data);
        this.setState({teamList:data});
        // this.props.appSetState({teamID:this.state.teamID,teamName:data.teamName,teamPass:this.state.teamPass,teamDesc:data.teamDesc,showTeamJoiner:false,teamAdmin:data.teamAdmin,teamAdminName:data.teamAdminName});
      }
      else{
        //console.log("error getting current team list");
      }
    }

    swapTeam(change){
      
      // change.persist();
      // //console.log(change.target.value);
      // //console.log(this.state.teamList);
      let team = this.state.teamList.find(team=>{return team.teamID===Number(change.target.value)});
      this.setState({teamID:team.teamID,teamPass:team.teamPass});
      
      // //console.log(team);
    }

    
  
    componentDidMount(){

      // if(this.props.noTeam===true){
        if(typeof getUrlVar("t")!=="undefined" && getUrlVar("t").length>0){
          this.setState({
            teamID:getUrlVar("t").split(":")[0],
            teamPass:getUrlVar("t").split(":")[1]
          },()=>{
            this.joinTeam();
          });
        }
        else if(typeof getUrlVar("teamID")!=="undefined" && typeof getUrlVar("teamPass")!=="undefined" && getUrlVar("teamID").length>0 && getUrlVar("teamPass").length>0){
          this.setState({
            teamID:getUrlVar("teamID"),
            teamPass:getUrlVar("teamPass")
          },()=>{
            this.joinTeam();
          });
        }
      // }


      
      
      
    }

    componentDidUpdate(prevProps,prevState){
      if(this.props.teamID!==prevProps.teamID){
        //console.log("trying to get existing teams...");
        this.getCurrentTeams();
      }
    }
  
    recordTeamDetails(change){
      // //console.log(allVals);
      let update={};
      update[change.target.name]=change.target.value;
  
      if(change.target.name==="joinlink"){

        if(typeof getUrlVar("t",change.target.value)!=="undefined" && getUrlVar("t",change.target.value).length>0){
          this.setState({
            teamID:getUrlVar("t",change.target.value).split(":")[0],
            teamPass:getUrlVar("t",change.target.value).split(":")[1]
          },()=>{
            this.joinTeam();
          });
        }
        else if(typeof getUrlVar("teamID",change.target.value)!=="undefined" && typeof getUrlVar("teamPass",change.target.value)!=="undefined" && getUrlVar("teamID",change.target.value).length>0 && getUrlVar("teamPass",change.target.value).length>0){
          this.setState({
            teamID:getUrlVar("teamID",change.target.value),
            teamPass:getUrlVar("teamPass",change.target.value)
          },()=>{
            this.joinTeam();
          });
        }
        else{
          message.error("That doesn't appear to be a valid team invite link...")
        }
      }
      else{
        this.setState({...update});
      }
  
  
      
    }
  
    render(){
      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 8 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 },
        },
      };
      const radioStyle = {
        display: 'block',
        height: '30px',
        lineHeight: '30px',
      };

      let returnValue=null;
      let titleText;
      if(this.props.showTeamJoiner===true){
        switch(this.state.mode){
          case "select":
              titleText = "Team Create/Join";

              if(!this.props.teamID){
                returnValue = (<>
                  <Row gutter={16}>
                      <Col span={12}>
                        <Card title="Create New Team" bordered={false} headStyle={{backgroundColor:"#88CC00"}} onClick={()=>{this.setState({mode:"create"})}} style={{cursor:"pointer"}} hoverable={true}>
                      
                          Create a new team if you were not invited by someone else to join their team, you can then invite others to your new team!
                        </Card>
                      </Col>
                      <Col span={12}>
                        <Card title="Join Existing Team" bordered={false} headStyle={{backgroundColor:"#0088EE"}} onClick={()=>{this.setState({mode:"join"})}} style={{cursor:"pointer"}} hoverable={true}>
                      
                          Join an existing team, using a team invite link someone else has provided you
                        </Card>
                      </Col>
                    </Row>
                </>
                );
              }
              else{
                returnValue = (<>
                  <Row gutter={16}>
                    <Col span={8}>
                      <Card title="Switch Between Teams" bordered={false} headStyle={{backgroundColor:"#E8A317"}} onClick={()=>{this.setState({mode:"switch"})}} style={{cursor:"pointer"}} hoverable={true}>
                    
                        Move to another team you're already a member of
                      </Card>
                    </Col>
                    <Col span={8}>
                      <Card title="Create New Team" bordered={false} headStyle={{backgroundColor:"#88CC00"}} onClick={()=>{this.setState({mode:"create"})}} style={{cursor:"pointer"}} hoverable={true}>
                    
                        Create a new team if you were not invited by someone else to join their team, you can then invite others to your new team!
                      </Card>
                    </Col>
                    <Col span={8}>
                      <Card title="Join Existing Team" bordered={false} headStyle={{backgroundColor:"#0088EE"}} onClick={()=>{this.setState({mode:"join"})}} style={{cursor:"pointer"}} hoverable={true}>
                    
                        Join an existing team, using a team invite link someone else has provided you
                      </Card>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={24}>
                      <Button onClick={()=>{this.props.appSetState({showTeamJoiner:false})}} style={{width:"100%",borderRadius:0,backgroundColor:cYellow,color:cBlue,border:0,fontWeight:"bold"}}>Cancel and Return To Team</Button>
                    </Col>
                  </Row>
                </>
                );
              }
              
            break;

            case "switch":
              titleText = "Switching Team";
              returnValue = (
                <Form onChange={this.swapTeam} onSubmit={()=>{this.joinTeam()}} {...formItemLayout}>
                  <Form.Item>
                    <Radio.Group name="teamID" value={Number(this.state.teamID) || this.props.teamID}>
                      {this.state.teamList.map((team,index)=>{return <Radio style={radioStyle} value={team.teamID}>{team.teamName}</Radio>})}
                    </Radio.Group>
                  </Form.Item>
                  <Form.Item >
                    <Button
                      type="primary"
                      htmlType="button"
                      disabled={this.state.serverComms}
                      onClick={()=>{this.setState({mode:"select"})}}
                      
                      
                    >
                      Back
                    </Button>
                    &nbsp; &nbsp;
                    <Button
                      type="primary"
                      htmlType="submit"
                      disabled={this.state.serverComms}
                    >
                      Confirm Selection
                    </Button>
                    
                    
                  </Form.Item>
                </Form>
              );

          
            break;





          case "create":
              titleText = "Creating New Team";
              returnValue =  (
              <>
                <Form onChange={this.recordTeamDetails} onSubmit={()=>{this.createTeam()}} {...formItemLayout}>
                  <Form.Item name="teamName" label="Team Name" rules={[{required:true, message:"Team name is required"}]} ><Input name="teamName" /></Form.Item>
                  {/* <Form.Item name="teamPass" label="Team Passcode" rules={[{required:true, message:"Team passcode is required"}]} ><Input type="password" name="teamPass" /></Form.Item> */}
                  <Form.Item name="teamDesc" label="Team Description" ><Input.TextArea placeholder={"Tell me about your team!"} name="teamDesc"/></Form.Item>
                  <Form.Item >
                    <Button
                      type="primary"
                      htmlType="button"
                      disabled={this.state.serverComms}
                      onClick={()=>{this.setState({mode:"select"})}}
                      
                      
                    >
                      Back
                    </Button>
                    &nbsp; &nbsp;
                    <Button
                      type="primary"
                      htmlType="submit"
                      disabled={this.state.serverComms}
                    >
                      Create Team
                    </Button>
                    
                    
                  </Form.Item>
                </Form>
              </>
            );
            break;
          case "join":
              titleText = "Joining Team";
              returnValue = (
              <Form onChange={this.recordTeamDetails} onSubmit={()=>{this.joinTeam()}} {...formItemLayout}>
              {/* <Form.Item name="teamID" label="Team Number" rules={[{required:true, message:"Team number is required"}]} ><Input name="teamID" /></Form.Item>
              <Form.Item name="teamPass" label="Team Passcode" rules={[{required:true, message:"Team passcode is required"}]}><Input type="password" name="teamPass" /></Form.Item> */}
              <Typography.Text>Paste a team link another team member has given you into the box below to join their team (It should look like "https://theukquarantinequiz.co.uk?t=XX:XXXXXXXXXX")</Typography.Text><br/>
              <Form.Item name="joinlink"  rules={[{required:true, message:"Team Invite Link is required"}]}> <Input name="joinlink" placeholder="Paste Team Invite Link Here"/> </Form.Item>
              <Form.Item >
                <Button
                  type="primary"
                  htmlType="button"
                  disabled={this.state.serverComms}
                  onClick={()=>{this.setState({mode:"select"})}}
                  
                >
                  Back
                </Button>
                &nbsp; &nbsp;
                <Button
                  type="primary"
                  htmlType="submit"
                  disabled={this.state.serverComms}
                >
                  Register
                </Button>
                
                
              </Form.Item>
            </Form>
            );

          
            break;
        }
        
        if(isBrowser){
          returnValue = (
            
              <Modal visible={true} centered={true} title={titleText} footer={null} width={this.props.teamID && this.state.mode==="select"?"50%":null} closable={false}>
                {returnValue}
              </Modal>
            
          );
          
        }

      }
        return returnValue;
      

    }
  }
















  export class VideoPlayer extends React.Component{
    constructor(props){
      super(props);
      this.state={
        
        vidLink:null
        
      }
      this.setState=this.setState.bind(this);
      this.connectFaye=this.connectFaye.bind(this);
      
    }
  
    async componentDidMount(){
      
      // fake loader
      
      
  
  
      
  
      let response = await fetch("./api/getvidlink?quizID="+this.props.quizID,{
        method:"GET",
        credentials:"same-origin",
        headers: {
          'Content-Type': 'application/json'
        }
      });
      if(response.ok){
        let data=await response.json();
        // //console.log(data.vidLink);
  
        this.props.stateSetter({vidLink:data.vidLink});
        
      }
  
      this.connectFaye();
  
      
  
    }


    componentDidUpdate(prevProps,prevState){
      if(prevProps.quizID!==this.props.quizID){
        this.connectFaye();
      }
    }
  
  
    connectFaye(){

      try{
        this.vidSubscription.cancel()
      }
      catch(err){}

      if(typeof this.props.quizID!=="undefined"){
        this.vidSubscription = fayeClient.subscribe("/"+this.props.quizID+"/vid",(vidLink)=>{
          //console.log("Video Link Incoming!");
          if(vidLink.quizID===this.props.quizID){
            if(vidLink.vidLink==="reload"){
              window.location.reload(true);
            }
            else{
              this.props.stateSetter({vidLink:vidLink.vidLink});
            }
          }
        });
      }
    }
  
    
  
    
  
    
  
   
   
    
  
    
  
    
  
    
  
    render(){
      
      let vidTag =<div style={{width:isBrowser?560:"100%"}}><img src={this.props.setup.coverUrl} style={{width:isBrowser?560:"100%"}} alt={"Next Quiz at 8PM"} /></div>;
  
      if(typeof this.props.vidLink!=="undefined" && this.props.vidLink!==null && this.props.vidLink!=="logo"){
        if(this.props.vidLink.includes("twitch")){
          
          vidTag=<>
            <iframe
              src={this.props.vidLink==="twitch"?"https://player.twitch.tv/?channel=thr33phase&autoplay=true&muted=false&parent=theukquarantinequiz.co.uk&parent=www.theukquarantinequiz.co.uk&parent=ukqq.co.uk&parent=www.ukqq.co.uk":this.props.vidLink+"&autoplay=true&muted=false&parent=theukquarantinequiz.co.uk&parent=www.theukquarantinequiz.co.uk&parent=ukqq.co.uk&parent=www.ukqq.co.uk"}
              height={315}
              width={isBrowser?560:"100%"}
              frameborder="0"
              // frameborder="<frameborder>"
              scrolling="no"
              allowfullscreen="false"
            />
          </>;
        }
        else if(this.props.vidLink.includes("youtube")){
          // https://www.youtube.com/embed/9VCt1uUn7V0
          // https://www.youtube.com/watch?v=9VCt1uUn7V0
          let youtubeEmbedLink;
          if(this.props.vidLink.includes("embed")){
            youtubeEmbedLink=this.props.vidLink;
          }
          else{
            youtubeEmbedLink="https://www.youtube.com/embed/"+this.props.vidLink.slice(this.props.vidLink.indexOf("v=")+2);
          }
          
          vidTag=<iframe width={isBrowser?560:"100%"} height={315} src={youtubeEmbedLink} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"/>;
        }
        else if(this.props.vidLink.includes("facebook.com")){
          let FBVidLink="https://www.facebook.com/plugins/video.php?href="+encodeURIComponent(this.props.vidLink)+"&width=560&show_text=false&height=315&appId"
          vidTag=<>
            


            <iframe src={FBVidLink} style={{border:"none",overflow:"hidden",width:"560px",height:"315px"}} scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>





          </>;
        }
        else{
          vidTag =<div style={{width:isBrowser?560:"100%",height:315}} className="content-outer"><div className="content-inner"><Title level={2} style={{color:"#FFFFFF"}}>{this.props.vidLink}</Title></div></div>;
        }
      }
  
      
        return(<>
          
            <div >
              
                {vidTag}
              
            </div>
            
          
          
        </>
      );
      
      
    }
  }

  export class ChatBox extends React.Component{
    constructor(props){
      super(props);
      this.state={
        
        chatMessages:[],
        
      }
      this.setState=this.setState.bind(this);
      
      this.sendChat=this.sendChat.bind(this);
      this.chatBoxRef=React.createRef();
      this.chatSendRef = React.createRef();
      
      this.connectFaye=this.connectFaye.bind(this);
      this.disconnectFaye=this.disconnectFaye.bind(this);
      
    }
  
    async componentDidMount(){
      
   
    }
  
  
    
  
    
  
    async componentDidUpdate(prevProps,prevState){
      
        if(this.props.teamID!==prevProps.teamID){
          if(typeof this.props.teamID!=="undefined"){
            if(this.props.teamID!==null){
              let chatMessageArray = [
                {user:"ThreePhase (Host)",text:"Welcome to the "+this.props.teamName+" teamchat, only your team (and the host) can see this so feel free to discuss the questions here, noone else will see it.",timestamp:moment().subtract(10,"second")},
                {user:"ThreePhase (Host)",text:"Your team captain is "+this.props.teamAdminName,timestamp:moment().subtract(8,"second")},
                {user:"ThreePhase (Host)",text:"You can message the host directly by including @host in your message, alternatively message @wife...",timestamp:moment().subtract(5,"second")},
                {user:"ThreePhase (Host)",text:"Your support would be welcomed! Tip me here -> ukqq.co.uk/tip",timestamp:moment().subtract(3,"second")}
              ];
              this.setState({chatMessages:chatMessageArray},()=>{
                // this.disconnectFaye();
                this.connectFaye();
              });
              
            }
            else{
              this.disconnectFaye();
            }
          }
          else{
            this.disconnectFaye();
          }
        }
      
      
    }

    componentWillUnmount(){
      this.disconnectFaye();
    }
  
    
  
   
   
    connectFaye(){
      this.disconnectFaye();
      //console.log("connecting chat channel on "+this.props.teamID);
      this.chatSubscription = fayeClient.subscribe("/"+this.props.quizID+"/chat/"+this.props.teamID,(message)=>{
        if(message.quizID===this.props.quizID){
          //console.log(message);
          let scrollBottom=this.chatBoxRef.current.scrollTop>=(this.chatBoxRef.current.scrollHeight-this.chatBoxRef.current.offsetHeight)-50;
          this.setState(currState=>{
            // sort messages into their date order
            let chatMessageSorted = [...currState.chatMessages,message].sort((a,b)=>moment(b.timestamp).isBefore(moment(a.timestamp))?1:-1);
            if(chatMessageSorted.length>30){
              //console.log("trimming old messages");
              chatMessageSorted=chatMessageSorted.slice(chatMessageSorted.length-30);
            }
            return {chatMessages:chatMessageSorted};
          },()=>{
            if(scrollBottom){this.chatBoxRef.current.scrollTop=this.chatBoxRef.current.scrollHeight-this.chatBoxRef.current.offsetHeight;}
            
          });
        }
      });
   
    }
  
    
  
    disconnectFaye(){
      try{
        this.chatSubscription.cancel();
        
      }
      catch(err){
        //console.log(err);
      }
    }
  
    
  
    async sendChat(){
      //console.log(this.chatSendRef.current);
      this.setState({chatSending:true});
      let sendMessage = this.chatSendRef.current.state.value;
      let response = await fetch("./api/chat",{
        method:"POST",
        credentials:"same-origin",
        headers: {
          'Content-Type': 'application/json'
        },
        body:JSON.stringify({
          userID:this.props.userID,
          userKey:this.props.userKey,
          userName:this.props.userName,
          message:sendMessage,
          timestamp:moment().format()
        })
      });
      //console.log(response);
      if(response.ok){
        this.setState({chatSending:false});
        this.chatSendRef.current.setState({value:""});
        this.chatSendRef.current.input.focus();
        
      }
      else{
        this.setState({chatSending:false});
        //console.log(response);
        message.error("Error sending chat message, check your connection & try again");
      }
    }
  
    
  
    render(){
      
      let returnValue = (<>

            <div className="chat-area" style={{color:isBrowser?"#ffffff":null,height:isBrowser?document.documentElement.clientHeight-(this.props.vidLink==="twitch"?364:355):500}}>
                <div ref={this.chatBoxRef} style={{height:isBrowser?document.documentElement.clientHeight-(this.props.vidLink==="twitch"?402:355):460,overflowY:"auto"}}>
                  {this.state.chatMessages.map((message,index)=>{return <div className="sider-chat-message" key={index}>{message.user} - {message.text}</div>})}
                </div>
                <Input.Group style={{position:"absolute",bottom:40,left:0,width:isBrowser?560:"100%"}} compact>
                  {/* <div style={{flex:"1 1 auto"}}> */}
                  <Input style={{position:"absolute",top:0,left:0,width:isBrowser?480:"calc(100% - 80px)",borderRadius:0}} ref={this.chatSendRef} placeholder={typeof this.props.teamID==="undefined" ? "Join a team to chat..." : "Say something..."} size="large" disabled={typeof this.props.teamID==="undefined"  || this.state.chatSending===true} onPressEnter={() => this.sendChat()}/>
                  {/* </div>
                  <div style={{flex:"none"}}> */}
                  <Button style={{position:"absolute",top:0,right:0,width:80,borderRadius:0,backgroundColor:cYellow,color:cDarkBlue,fontWeight:"bold"}} size="large" disabled={typeof this.props.teamID==="undefined" || this.state.chatSending===true} onClick={() => this.sendChat()} >Send</Button>
                  {/* </div> */}
                </Input.Group>
            </div>
          

      </>);

      try{
        //if(this.props.vidLink.includes("twitch")){
        if(this.props.vidLink==="twitch"){
          returnValue = (<>
            <Tabs defaultActiveKey="team" tabBarStyle={{color:cYellow,margin:0}}>
              <Tabs.TabPane tab={"Team Chat"} key="team" forceRender={true}>
                {returnValue}
              </Tabs.TabPane>
              
              <Tabs.TabPane tab={"Twitch Chat (Requires Twitch Login)"} key="global" forceRender={true}>
                <iframe frameborder="0"
                        scrolling="yes"
                        id="thr33phase"
                        src={this.props.vidLink==="twitch"?"https://www.twitch.tv/embed/thr33phase/chat?parent=theukquarantinequiz.co.uk":"https://www.twitch.tv/embed/"+this.props.vidLink.substr(this.props.vidLink.indexOf("=")+1)+"/chat&parent=theukquarantinequiz.co.uk"}
                        //src={this.props.vidLink==="twitch"?"https://www.twitch.tv/embed/thr33phase/chat?parent=theukquarantinequiz.co.uk":"https://www.twitch.tv/embed/"+this.props.vidLink.substr(this.props.vidLink.indexOf("=")+1,this.props.vidLink.substr(this.props.vidLink.indexOf("=")+1).indexOf("&"))+"/chat&parent=theukquarantinequiz.co.uk"}
                        height={isBrowser?document.documentElement.clientHeight-370:500}
                        width={isBrowser?560:"100%"}>
                </iframe>
              </Tabs.TabPane>
              
            </Tabs>
          
          </>);
        }
      }
      catch(err){
        //console.log(err)
      }
  
      return returnValue;
        
      
      
    }
  }

  export class TeamBar extends React.Component{
    constructor(props){
      super(props);
      this.state={
        
        teamMembers:[],
        
        showInviteMobile:false
      }
      this.setState=this.setState.bind(this);
      
      this.connectFaye=this.connectFaye.bind(this);
      this.disconnectFaye=this.disconnectFaye.bind(this);
      this.changeTeamName = this.changeTeamName.bind(this);
      this.getTeam = this.getTeam.bind(this);
      this.leaveTeam = this.leaveTeam.bind(this);
      this.updateTeamDetails = this.updateTeamDetails.bind(this);
    }
  
    async componentDidMount(){
      
      // fake loader
      
      
  
      setInterval(()=>{this.forceUpdate()},10000);
  
    }
  
  
    
  
    
  
    async componentDidUpdate(prevProps,prevState){
      
        if(this.props.teamID!==prevProps.teamID){
          if(typeof this.props.teamID!=="undefined"){
            if(this.props.teamID!==null){
              
              
                this.getTeam();
                this.connectFaye();
              
              
            }
            else{
              this.disconnectFaye();
            }
          }
          else{
            this.disconnectFaye();
          }
        }
      
      
    }
  
    async getTeam(){
      
      if(typeof this.props.teamID!=="undefined"){
        if(this.props.teamID!==null){
          //console.log("trying to get team...");
          
          let response = await fetch("./api/getteam",{
            method:"GET",
            credentials:"same-origin",
            headers: {
              'Content-Type': 'application/json'
            }
          });
          if(response.ok){
            let data=await response.json();
            // //console.log(data);

            //console.log(JSON.parse(JSON.stringify(data)));

            let teamMembership = [...data.members];
            delete data.members;

            let that=this;
            this.props.appSetState({...data},()=>{
              that.updateTeamDetails(teamMembership);
            });
            // this.props.appSetState({...data,userName:,loggedIn:true});
          }
        }
        
      }
      setTimeout(()=>{this.getTeam()},120000);
  
    }
  
    
  
   
  
    connectFaye(){
  
      this.disconnectFaye();
      
      if(typeof this.props.teamID!=="undefined"){
  
  
  
  
        if(!this.teamSubscription){
          this.teamSubscription = fayeClient.subscribe("/team/"+this.props.teamID,(teamDetail)=>{
            
            //console.log(JSON.parse(JSON.stringify(teamDetail)));
            // teamDetail.teamAdminName=teamDetail.userName;
            
            // delete teamDetail.userName

            if(this.props.teamAdminName!==teamDetail.teamAdminName){
              notification.open({
                message:"Change of team lead!",
                description:teamDetail.teamAdminName + " is now your team captain!"
              });
            }

            let teamMembership = [...teamDetail.members];
            delete teamDetail.members;

            let that=this;
            this.props.appSetState({...teamDetail},()=>{
              that.updateTeamDetails(teamMembership);
            });
            
            // this.setState(currState=>{
            //   let currentMembership = currState.teamMembers;
              
            //   for(let teamMember of currentMembership){
            //     teamMember.isCaptain=false;
            //     if(teamMember.userID===teamDetail.teamAdmin){
            //       teamMember.isCaptain=true;
            //     }
            //   }
            //   return {teamMembers:currentMembership};
            // })
            // this.updateTeamDetails(currentMembership)
          });
        }
  
        
  
        
  
      }
  
  
    }


    updateTeamDetails(membership){
      
      for(let member of membership){
        if(member.userID===this.props.teamAdmin){
          member.isCaptain=true;
        }
        else{
          member.isCaptain=false;
        }
        member.lastSeenText=moment(member.lastSeen).format();
      }

      let membersInTeam = membership.length;
      let membersOffline = membership.filter(member=>moment(member.lastSeen).isBefore(moment().subtract(10,"minutes"))).length;
      membership = membership.filter(member=>moment(member.lastSeen).isAfter(moment().subtract(10,"minutes")));
      if(membersOffline>0){
        membership.push({userID:99999,userName:membersOffline + " Offline Members",lastSeen:moment().subtract(11,"minutes"),isCaptain:false});
      }
      //console.log(membership);
      this.setState({teamMembers:membership});

    }
  
   
  
    disconnectFaye(){
      try{
        this.teamSubscription.cancel();
      }
      catch(err){
        //console.log(err);
      }
    }
  
    async setTeamLead(userID){
      if(this.props.userID===this.props.teamAdmin && typeof userID!=="undefined"){
        let response = await fetch("./api/setcaptain",{
          method:"POST",
          credentials:"same-origin",
          headers: {
            'Content-Type': 'application/json'
          },
          body:JSON.stringify({
            newAdmin:userID
          })
        });
        if(response.ok){
          message.success("Team Captain changed!");
          
        }
        else{
          message.error("Error changing captain, no connection or the selected team member is offline.");
        }
      }
    }


    async changeTeamName(name){
      if(name!==""){
        let response = await fetch("./api/teamname",{
          method:"POST",
          credentials:"same-origin",
          headers: {
            'Content-Type': 'application/json'
          },
          body:JSON.stringify({
            teamName:name
          })
        });
        if(response.ok){
          message.success("Team name changed!");
          
        }
        else{
          message.error("Unable to change team name...");
        }
      }
      else{
        message.error("You can't have a blank name!");
      }
    }


    async leaveTeam(){
      //console.log("leaving team...");
      // let response = await fetch("./api/leaveteam",{
      //   method:"GET",
      //   credentials:"same-origin",
      //   headers: {
      //     'Content-Type': 'application/json'
      //   },
        
      // });
      // if(response.ok){
      //   message.success("You have now left your old team and can join a new one, page will automatically reload now...");
      //   setTimeout(()=>{document.location.reload(true)},5000);
      // }
      // else{
      //   message.error("Server error");
      // }
      this.props.appSetState({showTeamJoiner:true});
    }
  
    render(){
      
        let shareUrl = "http://ukqq.co.uk?t="+this.props.teamID+":"+this.props.teamPass;
        let shareText = "Come join my team on The UK Quarantine Quiz!";
      
        return(<>
          
              <div style={{flex:"0 1 auto"}}>
                <Card bodyStyle={{padding:10,backgroundColor:cDarkBlue}} bordered={false}>
                  {/* {this.state.teamMembers.length>0? */}
                    <Row type="flex" justify="space-around" align="top">
                      <Col xs={24} sm={16} md={14} lg={12} > 
                        {/* <Text strong style={{color:cYellow,fontSize:"1.3em"}} >Your Team - </Text><Text editable={{ onChange: this.changeTeamName }} strong style={{color:cYellow,fontSize:"1.3em"}} >{this.props.teamName}</Text><Popconfirm title={"Are you sure you want to leave your team?"} onConfirm={this.leaveTeam} okText={"Yes, leave my team"} ><Button style={{marginLeft:"10px",color:cBlue,backgroundColor:cYellow,borderRadius:0,border:0,fontWeight:"bold"}}>Leave Team</Button></Popconfirm><p/> */}
                        <Text strong style={{color:cYellow,fontSize:"1.3em"}} >Your Team - </Text><Text editable={{ onChange: this.changeTeamName }} strong style={{color:cYellow,fontSize:"1.3em"}} >{this.props.teamName}</Text><Button style={{marginLeft:"10px",color:cBlue,backgroundColor:cYellow,borderRadius:0,border:0,fontWeight:"bold"}} onClick={()=>{this.leaveTeam()}}>Change Team</Button><p/>
                          {this.state.teamMembers.map((teamMember,index)=>{
                            let iconColor = "#10BB00";
                            if(teamMember.isCaptain===true){
                              iconColor = "#FFD700";
                            }
                            if(moment(teamMember.lastSeen).isBefore(moment().subtract(10,"minutes"))){
                              iconColor="#CCCCCC";
                            }

                            return (
                              <Tag color={iconColor} onClick={()=>{this.setTeamLead(teamMember.userID)}}  style={{cursor:"pointer",borderRadius:0,border:0}} key={index}>
                              {/* <Tag color={iconColor} onClick={()=>{this.setTeamLead(teamMember.userID)}}> */}
                                <Icon type={teamMember.isCaptain===true?"star":"user"} theme={teamMember.isCaptain===true?"filled":null} style={{color:cDarkBlue}} /> <Text strong={true} style={{color:cDarkBlue}}>{teamMember.userName}</Text>
                              </Tag>
                            )
                          })}
                      </Col>
                    {/* </Row>
                    <br/>
                    <Row type="flex" justify="space-around" align="middle"> */}
                    <Col xs={24} sm={8} md={10} lg={12} style={{verticalAlign:"middle"}}>
                        <Title level={4} style={{color:cYellow}}>Invite people to join your team :</Title>
                        <FacebookShareButton url={shareUrl} quote={shareText}><FacebookIcon size={32} /></FacebookShareButton>
                        <TwitterShareButton url={shareUrl} title={shareText}><TwitterIcon size={32} /></TwitterShareButton>
                        <WhatsappShareButton url={shareUrl} title={shareText}><WhatsappIcon size={32} /></WhatsappShareButton>
                        {/* <InstapaperShareButton url={shareUrl} title={shareText}><InstapaperIcon size={32} /></InstapaperShareButton> */}
                        <TelegramShareButton url={shareUrl} title={shareText}><TelegramIcon size={32} /></TelegramShareButton>
                        <EmailShareButton url={shareUrl} subject={shareText}><EmailIcon size={32} /></EmailShareButton>
                        <br/>
                        <Text style={{whiteSpace:"nowrap",padding:"3px",backgroundColor:cYellow,color:"#0e2b56"}} id={"copybit"} data-clipboard-text={shareUrl} onClick={()=>{message.success("Copied Team Link")}}>{shareUrl}</Text>
                    </Col>
                    </Row>
                  {/* :null} */}

                  {/* {typeof this.props.teamID!==undefined?<Button style={{marginLeft:"10px",color:"#0000FF"}} onClick={this.inviteModal}  onTouch={this.inviteModal}>Invite Others</Button>:null}
                  
                  {this.state.showInviteMobile ? <><br/><Typography.Text>Copy and give this link to your friends so they can join your team - </Typography.Text><Typography.Text copyable={true} type={"secondary"}>{"https://www.theukquarantinequiz.co.uk?teamID="+this.props.teamID+"&teamPass="+this.props.teamPass}</Typography.Text></> : null} */}
                  
                


                </Card>
              </div>
        </>);
      
      
      
    }
  }

  export class Quiz extends React.Component{
    constructor(props){
      super(props);
      this.state={
        questionID:null,
        questions:{},
        answerDisabled:true,
      }
      this.setState=this.setState.bind(this);
      this.suggestAnswer=this.suggestAnswer.bind(this);
      this.sendAnswer=this.sendAnswer.bind(this);
      this.answerSendRef = React.createRef();
      this.connectFaye=this.connectFaye.bind(this);
      this.disconnectFaye=this.disconnectFaye.bind(this);
      this.imgRef = React.createRef();
      this.resizeImg=this.resizeImg.bind(this);
      this.LBRef = React.createRef();
      this.contentRef = React.createRef();
      this.countDown=this.countDown.bind(this);
    }
  
    
  
  
    
  
    
  
    async componentDidUpdate(prevProps,prevState){
      
        if(this.props.teamID!==prevProps.teamID){
          if(typeof this.props.teamID!=="undefined"){
            if(this.props.teamID!==null){
              
              //console.log("connecting FAYE");
                
                this.connectFaye();
              
              
            }
            else{
              this.disconnectFaye();
            }
          }
          else{
            this.disconnectFaye();
          }
        }

        if(typeof this.props.quizStatus!=="undefined" && typeof prevProps.quizStatus==="undefined"){
          let quizStatusDeNulled = this.props.quizStatus.filter(q=>q!==null);
          if(quizStatusDeNulled.length>0){
            if(quizStatusDeNulled.findIndex(q=>{return q.isActive===1})>-1){
              this.setState({questions:quizStatusDeNulled,questionID:quizStatusDeNulled.find(q=>{return q.isActive===1}).questionID,answerDisabled:false,countDown:moment().subtract(12,"seconds").toDate()},()=>{this.countDown();});
            }
            else{
              this.setState({questions:quizStatusDeNulled,questionID:0,answerDisabled:false,countDown:moment().subtract(12,"seconds").toDate()},()=>{this.countDown();});
            }
          }
        }
      
      
    }
  
    
  
    
  
    countDown(){
      if(typeof this.cdTimer==="undefined"){
        
        this.cdTimer = setInterval(()=>{
          if(moment(this.state.countDown).isAfter(moment().subtract(10,"seconds"))){
            this.forceUpdate();
          }
          else{
            clearInterval(this.cdTimer);
            this.cdTimer=undefined;
          }
        },1000);
      }
    }
  
    connectFaye(){
  
      this.disconnectFaye();
      
      // if(typeof this.props.teamID!=="undefined"){
  
  
  
        
        
  
        
  
        if(!this.suggestionSubscription){
          //console.log("creating subscription to suggestions channel");
          this.suggestionSubscription = fayeClient.subscribe("/"+this.props.quizID+"/suggestion/"+this.props.teamID,(answer)=>{
            if(answer.quizID===this.props.quizID){
              //console.log("incoming suggestion!");
              //console.log(answer);
              this.setState(currState=>{
                //console.log(currState.questions);
                let currentQuestions;

                if(Array.isArray(currState.questions)){
                  currentQuestions=[...currState.questions];
                }
                else{
                  currentQuestions=[];
                }


                if(currentQuestions.findIndex(q=>q.questionID===answer.questionID)!==-1){
                  // let currentQuestions={...currState.questions};
                  //console.log("here")
                  let currentQuestion=currentQuestions.find(q=>q.questionID===answer.questionID);
                  let clearQuestion;
                  if(answer.selected===true){
                      // for(let question of Object.keys(currentQuestions)){
                      //   //console.log(question);
                      //   //console.log(JSON.stringify(currentQuestions[question]));
                        
                        for (let suggestion in currentQuestion.suggestions){
                          
                          currentQuestion.suggestions[suggestion].selected=false;
                        }
                        // currentQuestions[question]=clearQuestion;
                      // }
                  }
                  // see if answer already in list, add name to it if it is, otherwise add it.
                  let existingAnswer = currentQuestion.suggestions.findIndex(suggestion=>suggestion.answer===answer.answer);
                  if(existingAnswer!==-1){
                    // if(!currentQuestion.suggestions[existingAnswer].userName.includes(answer.user)){
                      currentQuestion.suggestions[existingAnswer].userName = answer.user;
                    // }
                    
                    
                    if(currentQuestion.suggestions[existingAnswer].selected!==true){
                      currentQuestion.suggestions[existingAnswer].selected = answer.selected;
                    }
                  }
                  else{
                    currentQuestion.suggestions.push({answer:answer.answer.substring(0,100),userName:answer.user,selected:answer.selected});
                  }
          
                  currentQuestions[currentQuestions.findIndex(q=>q.questionID===answer.questionID)] = currentQuestion;
                  //console.log(currentQuestion);
                  return {questions:currentQuestions};
                }
                else{
                  return {};
                }
              });
            }
          });
        }
  
        if(!this.answerSubscription){
          //console.log("creating subscription to answers channel");
          this.answerSubscription = fayeClient.subscribe("/"+this.props.quizID+"/answer/"+this.props.teamID,(question)=>{
            if(question.quizID===this.props.quizID){
              //console.log("incoming answer!");
              //console.log(question)

              // let currQs={...this.state.questions};
              let currentQuestions;

                if(Array.isArray(this.state.questions)){
                  currentQuestions=[...this.state.questions];
                }
                else{
                  currentQuestions=[];
                }

              if(this.LBRef.current!==null){
                this.LBRef.current.hideSelf();
              }
              
                
              if(typeof question.img==="undefined"){
                question.img="";
              }
              let qNew=true;
              // //console.log(question);
      
              this.setState(currState=>{
                let currentQuestions;

                if(Array.isArray(this.state.questions)){
                  currentQuestions=[...this.state.questions];
                }
                else{
                  currentQuestions=[];
                }
                
                if(currentQuestions.findIndex(q=>q.questionID===question.questionID)!==-1){
                  question.suggestions=currentQuestions.find(q=>q.questionID===question.questionID).suggestions;
                  qNew=false;
                  currentQuestions[currentQuestions.findIndex(q=>q.questionID===question.questionID)]=question;
                }
                else{
                  question.suggestions=[];
                  currentQuestions.push(question);
                }
      
                
                  
                
    
                return {questions:currentQuestions,questionID:question.questionID,answerDisabled:qNew};
              },()=>{
                if(isMobile){
                  // window.scrollTo(document.body,0,this.contentRef.current.offsetTop);
                  window.scrollTo(0,this.contentRef.current.offsetTop);
                  // this.contentRef.current.scrollIntoView();
                }
              });
      
                  
                  
                  
              // }
            
            }
          });
          
        }
  
      // }
  
  
  
  
  
      
  
  
  
  
      
      if(!this.questionSubscription){
        //console.log("creating subscription to questions channel");
        this.questionSubscription = fayeClient.subscribe("/"+this.props.quizID+"/question",(question)=>{
          if(question.quizID===this.props.quizID){
            //console.log("incoming question!");
            //console.log(question);
            let currQs;

            if(Array.isArray(this.state.questions)){
              currQs=[...this.state.questions];
            }
            else{
              currQs=[];
            }

            if(this.LBRef.current!==null){
              this.LBRef.current.hideSelf();
            }
            if(currQs.findIndex(q=>q.questionID===question.questionID)===-1 && question.answerMode!==true){
              this.setState({countDown:moment().toDate()});
              this.countDown();
              setTimeout((question)=>{
                if(typeof question.img==="undefined"){
                  question.img="";
                }
                let qNew=true;
                // //console.log(question);
        
                this.setState(currState=>{

                  let currentQuestions;

                  if(Array.isArray(currState.questions)){
                    currentQuestions=[...currState.questions];
                  }
                  else{
                    currentQuestions=[];
                  }
                  
                  if(currentQuestions.findIndex(q=>q.questionID===question.questionID)!==-1){
                    question.suggestions=currentQuestions.find(q=>q.questionID===question.questionID).suggestions;
                    currentQuestions[currentQuestions.findIndex(q=>q.questionID===question.questionID)]=question;
                    qNew=false;
                  }
                  else{
                    question.suggestions=[];
                    currentQuestions.push(question);
                  }
      
                  
                  
                  
                  return {questions:currentQuestions,questionID:question.questionID,answerDisabled:qNew};
                },()=>{
                  if(isMobile){
                    window.scrollTo(0,this.contentRef.current.offsetTop);
                    // window.scrollTo(0, this.myRef.current.offsetTop)
                    // this.contentRef.current.scrollIntoView();
                  }
                });
                
                
                
                //console.log(question);
                if(question.answerMode!==true){
                  //console.log("answer mode...");
                  if(qNew){setTimeout(()=>{this.setState({answerDisabled:false})},3000);}
                }
              },10000,question);
              
            }
            else{
              
                if(typeof question.img==="undefined"){
                  question.img="";
                }
                let qNew=true;
                // //console.log(question);
        
                this.setState(currState=>{
                  let currentQuestions;

                  if(Array.isArray(currState.questions)){
                    currentQuestions=[...currState.questions];
                  }
                  else{
                    currentQuestions=[];
                  }
                  
                  if(currentQuestions.findIndex(q=>q.questionID===question.questionID)!==-1){
                    question.suggestions=currentQuestions.find(q=>q.questionID===question.questionID).suggestions;
                    currentQuestions[currentQuestions.findIndex(q=>q.questionID===question.questionID)]=question;
                    qNew=false;
                  }
                  else{
                    question.suggestions=[];
                    currentQuestions.push(question);
                  }
      
                  
                  
                  
                  return {questions:currentQuestions,questionID:question.questionID,answerDisabled:qNew};
                },()=>{
                  if(isMobile){
                    // window.scrollTo(document.body,0,this.contentRef.current.offsetTop);
                    window.scrollTo(0,this.contentRef.current.offsetTop);
                    // this.contentRef.current.scrollIntoView();
                  }
                });
    
                
                
                //console.log(question);
                if(question.answerMode!==true){
                  //console.log("answer mode...");
                  if(qNew){setTimeout(()=>{this.setState({answerDisabled:false})},3000);}
                }
            }
          }
        });
          
  
  
          
          
          
  
        
      }
      
      
    }
  
    resizeImg(){
      if(this.state.img!==""){
        //console.log(this.imgRef.current.height + "x" + this.imgRef.current.width);
        try{
          if(this.imgRef.current.height>this.imgRef.current.width){
            //console.log("H");
            this.imgRef.current.style.height="50%";
            this.imgRef.current.style.maxHeight="300px";
          }
          else{
            //console.log("W");
            this.imgRef.current.style.width="50%";
            this.imgRef.current.style.maxWidth="300px";
          }
        }
        catch(err){}
        //console.log("now " + this.imgRef.current.height + "x" + this.imgRef.current.width);
      }
    }
  
    disconnectFaye(){
      try{
        this.suggestionSubscription.cancel();
        // this.questionSubscription.cancel();
        // this.answerSubscription.cancel();
        
      }
      catch(err){
        //console.log(err);
      }
      try{
        // this.suggestionSubscription.cancel();
        this.questionSubscription.cancel();
        // this.answerSubscription.cancel();
        
      }
      catch(err){
        //console.log(err);
      }
      try{
        // this.suggestionSubscription.cancel();
        // this.questionSubscription.cancel();
        this.answerSubscription.cancel();
        
      }
      catch(err){
        //console.log(err);
      }
    }
  
    async suggestAnswer(answer){
      if(this.state.answerDisable!==true){
        this.setState({answerDisabled:true});
        let response = await fetch("./api/suggest",{
          method:"POST",
          credentials:"same-origin",
          headers: {
            'Content-Type': 'application/json'
          },
          body:JSON.stringify({
            questionID:this.state.questionID,
            roundNumber:this.state.roundNumber,
            questionNumber:this.state.questionNumber,
            answer:answer.substring(0,100)
          })
        });
        if(response.ok){
          message.success("Suggestion Sent!");
          this.answerSendRef.current.input.setValue("");
          
          this.setState({answerDisabled:false});
          this.answerSendRef.current.input.focus();
          // this.props.appSetState({...data,userName:,loggedIn:true});
        }
        else{
          message.error("Suggestion Error, Check Your Internet Connection & Try Again");
          this.setState({answerDisabled:false});
          this.answerSendRef.current.input.focus();
        }
      }
    }
  
    
    
  
    async sendAnswer(answer){
      if(this.state.answerDisabled!==true){
        if(this.props.userID===this.props.teamAdmin){
          // this.setState({answerDisabled:true});
          let response = await fetch("./api/answer",{
            method:"POST",
            credentials:"same-origin",
            headers: {
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({
              questionID:this.state.questionID,
              roundNumber:this.state.roundNumber,
              questionNumber:this.state.questionNumber,
              answer:answer.substring(0,100)
            })
          });
          if(response.ok){
            message.success("Answer Sent!");
            // this.answerSendRef.current.input.setValue("");
            // this.setState({answerDisabled:false})
            // this.props.appSetState({...data,userName:,loggedIn:true});
          }
          else{
            message.error("You're either not connected or you're not the team captain... either way you can't answer for the team!");
            // this.setState({answerDisabled:false});
          }
        }
        else{
          // if(this.state.answerDisabled!==true){
            this.suggestAnswer(answer);
          // }
        }
      }
    }
  
    
  
    
  
    render(){
      let returnValue;
      try{

        let question;
        try{
          question = this.state.questions.find(q=>q.questionID===this.state.questionID);

        }
        catch(err){
          //console.log(err)
        }

        //console.log(question);
        
        if(typeof question!=="undefined"){
          returnValue =(<>
              
              
                <div className={"content-outer"} ref={this.contentRef} style={{padding:"20px",flex:"1 1 auto",backgroundColor:"#FFFFFF"}}>
                {/* <div> */}
                  
                  
                    {
                      question.questionNumber!==null && question.roundNumber!==null ? 
                        <>
                        {/* <QuestionSelector questions={this.state.questions} current={this.state.questionID}/> */}
                        <div className={"content-inner"}  >
                        
                        <Title level={isBrowser?2:4}>Round {question.roundNumber}</Title>
                        <p/>
                        <Title level={isBrowser?1:3}>Question {question.questionNumber}</Title>
                        {question.img!=="" && question.img!==null?<Image ref={this.imgRef} src={question.img} onLoad={this.resizeImg} retry={{count:10,delay:3,accumulate:'noop'}}/>:null}
                        <p/>&nbsp;
                        <p/>
                        <Title level={isBrowser?2:4}>{question.question}</Title>
                        <p/>&nbsp;
                        {this.props.loggedIn===true ? 
                          (this.props.teamID!==null ? 
                            (
                              question.answerMode===false ?
                              <><Input.Search ref={this.answerSendRef} placeholder="What's your answer?" enterButton={this.state.answerDisabled ? false : (moment(this.state.countDown).isAfter(moment().subtract(10,"seconds")) ? "Next Q in "+ Math.round(10- moment.duration(moment().diff(moment(this.state.countDown))).asSeconds()) :"Suggest Answer")} size="large" disabled={this.state.answerDisabled} loading={this.state.answerDisabled} onSearch={value => this.suggestAnswer(value)}/></>
                              // <><Input.Search ref={this.answerSendRef} placeholder="What's your answer?" enterButton={this.state.answerDisabled ? false : (this.state.countDown.isAfter(moment().subtract(10,"seconds")) ? "Hurry Up!" :"Suggest Answer")} size="large" disabled={this.state.answerDisabled} loading={this.state.answerDisabled} onSearch={value => this.suggestAnswer(value)}/></>
                              :
                              null
                            )
                            
                          :
                            "You need to create or join a team to answer questions..."
                          )
                        :
                          "Create an account and join a team to answer questions... or login if you already have!"
                        }
                        <p/>
                      </div>
                      <div className={"content-inner"}  >
                        {question.answerMode===false?
                          <SuggestionsArea suggestions={question.suggestions} sendAnswer={this.sendAnswer} isCaptain={this.props.isCaptain} teamAdminName={this.props.teamAdminName}  /> 
                        :
                          <AnswerDisplay actualAnswer={question.actualAnswer} teamAnswer={question.teamAnswer} isCorrect={question.correct===1} />
                        }
                        
                        <p/>
                        
                        
                        {question.answerMode===false ? (this.props.userID===this.props.teamAdmin ? <>You are the team captain! As well as making suggestions you can pick one of your teams answers above to submit it for scoring.</> : <>Suggest answers and vote on your teams answers, the team captain ({this.props.teamAdminName}) will then submit an answer for scoring.</>) : <>Going through answers... wait for next question!</>}
                        {/* <p/>{JSON.stringify(this.props)} */}
                        </div>
                        </>
                      :
                      <div className={"content-inner"}  >
                        {this.props.loggedIn===true ? <><Title level={1}>Waiting for a question...<p/>Quiz starts at 8pm!</Title><QuizSwitcher {...this.props}/></> : <><Title level={1}>Create an account to join in!</Title></>}
                      </div>
                    }
                    
                    
                  {/* </div> */}
                  <Leaderboard placement={"right"} ref={this.LBRef} teamID={this.props.teamID} quizID={this.props.quizID}/>
                </div>
                
            </>
          );
        }
        else{
          returnValue = (<>
            <div className={"content-outer"}  ref={this.contentRef} style={{padding:"20px",flex:"1 1 auto",backgroundColor:"#FFFFFF"}}>
              {/* <div style={{flex:"1 1 auto"}}> */}
                <Title level={isBrowser?1:3}>Awaiting Question from Host</Title>
                <Leaderboard placement={"right"} ref={this.LBRef} teamID={this.props.teamID} quizID={this.props.quizID}/>
              {/* </div> */}
            </div>
          </>);
        }
      }
      catch(err){
        //console.log(err);
        returnValue=<>error - {JSON.stringify(err)}</>;
      }
      
      return returnValue;

    }
    
  }



export class QuestionSelector extends React.Component{
  constructor(props){
    super(props);
    this.state={};

  }

  render(){
    let returnValue=null;
    if(typeof this.props.questions!=="undefined" && this.props.questions.length>0){
      //console.log(this.props.questions)
      let currentQuestion=this.props.questions.find(q=>{return (q!==null ? q.questionID===this.props.current : false)});
      //console.log(currentQuestion);
      returnValue=(
      <Steps current={currentQuestion.roundNumber}>
        {this.props.questions.filter(q=>q!==null).map(q=>q.roundNumber).reduce((unique,item)=>{return unique.includes(item)?unique:[...unique,item]},[]).map(round=>{return <Steps.Step title={"Round "+round} key={round}/>})}
      </Steps>
      );
    }

    return returnValue;
  }


}




export class SuggestionsArea extends React.Component{



render(){
  return (<>
    
        <Divider>{
            this.props.isCaptain ?
              <>Below are your teams suggestions{isBrowser?", c":<><br/> C</>}lick one to submit it for marking!</>
            :
              <>Here's your team suggestions{isBrowser?", c":<><br/> C</>}lick to add your vote so {this.props.teamAdminName} can select an answer to be marked!</>
        }</Divider>
        <Card title={this.props.suggestions.length>0?null:"No suggestions yet..."}>
          <Row gutter={[5,5]} type={"flex"} align="middle">
          {
          this.props.suggestions.map((suggestion,index)=>{
           return (<Col span={isMobile?24:8}>
             {/* <Card.Grid
             style={{
               width:"100%",
              //  height:"100%",
               textAlign:"center",
               backgroundColor:suggestion.selected===true?cYellow:cBlue,
               cursor:"pointer"
              }} 
             key={index}
             
             hoverable={true}
            //  onTouch={()=>{this.props.sendAnswer(suggestion.answer)}}
             > */}
              <Row type={"flex"} align={"middle"} justify={"center"} style={{
               width:"100%",
               height:"100%",
               padding:"10px",
               textAlign:"center",
               backgroundColor:suggestion.selected===true?cYellow:cBlue,
               cursor:"pointer"
              }}
              onClick={()=>{this.props.sendAnswer(suggestion.answer)}}
              >
                <Col span={1}>
                  <Title level={1} style={{color:cBlue}}>{suggestion.selected?<>&#187;</>:<>&nbsp;</>}</Title>
                </Col>
                <Col span={22}>
                  <Title level={2} style={{color:suggestion.selected===true?cBlue:cYellow}}>
                    {suggestion.answer}
                  </Title>
                  <p/>
                  <Text style={{color:suggestion.selected===true?cBlue:cYellow}}>By {suggestion.userName}</Text>
                </Col>  
                <Col span={1}>
                <Title level={1} style={{color:cBlue}}>{suggestion.selected?<>&#171;</>:<>&nbsp;</>}</Title>
                </Col>
              </Row>
            {/* </Card.Grid> */}
            </Col>
            )
          })
          }
          </Row>
        </Card>
      
  </>);
}


}

export class AnswerDisplay extends React.Component{



  render(){
    return (<>
      
          
            <Title level={1}>The actual answer is {this.props.actualAnswer}</Title>
            <br/>
            <Title level={1} style={{color:this.props.isCorrect===true?"#88FF00":"#CC4400"}}>
              {this.props.teamAnswer !== false ?
                <>Your team answered with {this.props.teamAnswer} which was {this.props.isCorrect===true?"Correct!":"Incorrect... :("}</>
              :
                <>Your team didn't get an answer in in time! :(</>
              }
            </Title>
            
          
    </>);
  }
  
  
}








  export class Leaderboard extends React.Component{
    constructor(props){
      super(props);
      this.state={
        visible:false
        
      }
      
      this.hideSelf = this.hideSelf.bind(this);
      this.connectFaye=this.connectFaye.bind(this);
    }
  
  
    componentDidMount(){
    
      this.connectFaye();
      
      
  
    }

    componentDidUpdate(prevProps,prevState){
      if(this.props.quizID!==prevProps.quizID){
        this.connectFaye();
      }
    }



    connectFaye(){
      try{
        this.leaderBoardSubscription.cancel()
      }
      catch(err){}
        this.leaderBoardSubscription=fayeClient.subscribe("/"+this.props.quizID+"/leaderboard",(lbdata)=>{
          if(lbdata.data && lbdata.quizID===this.props.quizID){
            //console.log(lbdata);
            this.setState({lbdata:lbdata.data,visible:true});
          }
        });
      
    }

    componentWillUnmount(){
      this.leaderBoardSubscription.cancel();
    }
  
    hideSelf(){
      this.setState({visible:false});
    }
  
  
    render(){
     
      return(
        <Drawer
          // title="Basic Drawer"
          placement={this.props.placement}
          width={"100%"}
          height={"100%"}
          closable={false}
          // onClose={this.onClose}
          visible={this.state.visible}
          // getContainer={this.props.contentRef.current}
          getContainer={false}
          style={{ position: 'absolute',minHeight:"50vh" }}
        >
          <Title level={1}>Leaderboard</Title>
          <Table dataSource={this.state.lbdata} pagination={false} scroll={false} columns={
            [
              {title:"Position",dataIndex:"teamName",key:"teamName",align:"center",render:(teamName,rowRecord,rowIndex)=>{console.log(rowRecord);return rowIndex===0?1:(rowRecord.score===this.state.lbdata[rowIndex-1].score?"\"":rowIndex+1)}},
              {title:"Team",dataIndex:"teamName",key:"teamName",render:(teamName,rowRecord)=>{return <Typography.Text style={{backgroundColor:typeof this.props.teamID!=="undefined"?(rowRecord.teamID===this.props.teamID?cYellow:null):null}}>{teamName}</Typography.Text>}},
              {title:"Points",dataIndex:"score",key:"teamName",align:"center"}
            ]
          }/>
        </Drawer>
      );
      
    }
  }

//   module.exports.Quiz = Quiz;