import React from 'react';
// import ReactDOM from 'react-dom';
import logo from './logo.svg';
import './App.css';
import 'antd/dist/antd.css';
import {Layout,Typography,Form, Input,InputNumber, Button, Checkbox,Row,Col,Modal,Affix,message,Card,List,Table,Tabs, Popconfirm,Collapse,Upload} from 'antd';
import { UserOutlined, LockOutlined, MailOutlined } from '@ant-design/icons';
import faye from 'faye';
import moment from 'moment';
// import { suggest } from '../../quarantine-quiz-server/quizApi';


const {Title}=Typography;
const { Header, Content, Sider } = Layout;
// let fayeClient = new faye.Client(window.location.protocol+"//"+window.location.hostname+":30001/websocket");
let fayeClient = new faye.Client("../websocket");
// fayeClient.disable("websocket");
let userID;
let userKey;
fayeClient.addExtension({
  outgoing: function(message, callback) {
    message.ext = message.ext || {};
    message.ext.userID = userID;
    message.ext.userKey = userKey;
    callback(message);
  },
  incoming: function(message,callback){
    //console.log(message);
    callback(message);
  }
});

class App extends React.Component{
  constructor(props){
    super(props);
    this.state={
      chatMessages:[],
      specialMessages:[],
      chatChannel:0,
      questions:[],
      currentQuestion:null,
      leaderBoard:[],
      activeTabRight:"1"
    }
    this.chatBoxRef=React.createRef();
    this.chatSendRef=React.createRef();
    this.vidSendRef=React.createRef();
    this.sendVidLink=this.sendVidLink.bind(this);
    this.sendChat=this.sendChat.bind(this);
    this.updateLeaderboard=this.updateLeaderboard.bind(this);
    this.forceCorrect=this.forceCorrect.bind(this);
    this.askQuestion=this.askQuestion.bind(this);
    this.answerQuestion=this.answerQuestion.bind(this);
    this.getQuestions=this.getQuestions.bind(this);
    this.changeCaptain=this.changeCaptain.bind(this);
    this.changePanelRight=this.changePanelRight.bind(this);
    this.stateSetter=this.setState.bind(this);
    this.addQuestion=this.addQuestion.bind(this);
    this.addRound=this.addRound.bind(this);
    this.remarkQuestion=this.remarkQuestion.bind(this);
    this.newQuiz=this.newQuiz.bind(this);
  }


  async componentDidMount(){
    
    let response = await fetch("../auth/query",{credentials:"same-origin"});
    if(response.ok){
      let data = await response.json();
      //console.log(data);
      userID=data.userID;
      userKey=data.userKey;
      if(!data.teamID){
        data.noTeam=true;
      }
      
      this.setState({...data,loggedIn:true},()=>{
        this.chatSubscription = fayeClient.subscribe("/"+this.state.quizID+"/chat/*",(message)=>{
          
          if(message.quizID===this.state.quizID){
            let scrollBottom=this.chatBoxRef.current.scrollTop>=(this.chatBoxRef.current.scrollHeight-this.chatBoxRef.current.offsetHeight)-50;
            this.setState(currState=>{
              // sort messages into their date order
              let chatMessageSorted = [...currState.chatMessages,message].sort((a,b)=>moment(b.timestamp).isBefore(moment(a.timestamp))?0:-1).slice(currState.chatMessages.length-100);
              let specialMessagesSorted = [...currState.specialMessages,message].filter(message=>{return message.text.toLowerCase().includes("@host") || message.text.toLowerCase().includes("@wife")}).sort((a,b)=>moment(b.timestamp).isBefore(moment(a.timestamp))?0:-1).slice(currState.chatMessages.length-100);
              return {chatMessages:chatMessageSorted,specialMessages:specialMessagesSorted};
            },()=>{
              if(scrollBottom){this.chatBoxRef.current.scrollTop=this.chatBoxRef.current.scrollHeight-this.chatBoxRef.current.offsetHeight;}
              
            });
          }
          

          
        });
        // this.suggestSubscription = fayeClient.subscribe("/"+this.state.quizID+"/suggestion/*",(suggestion)=>{
        //   if(suggestion.quizID===this.state.quizID){
        //     //console.log(suggestion);
        //     this.setState(currState=>{
        //       let currQs=currState.questions;
        //       let qIndex=currQs.findIndex(q=>{return q.questionID===suggestion.questionID})
        //       let currSs=currQs[qIndex].suggestions;
        //       let aIndex=currSs.findIndex(currS=>currS.answer===suggestion.answer && currS.teamID===suggestion.teamID);
        //       if(aIndex===-1){
              
        //         currSs.push(suggestion);

        //         currQs[qIndex].suggestions=currSs.slice(currSs.length-20);
                
        //       }
        //       return {questions:currQs};
        //     });
        //   }
        // });

        this.answerSubscription = fayeClient.subscribe("/"+this.state.quizID+"/host/answers",(answers)=>{
          //console.log(answers.data);
          this.setState(currState=>{
            let currQs=currState.questions;
            console.log(answers);
            try{
              for (let answer of answers.data){
                if(answer.quizID===currState.quizID){
                  //console.log("here")
                  //console.log(answer);

                  
                    try{
                      
                      let qIndex=currQs.findIndex(q=>{return q.questionID===answer.questionID});
                      //console.log(qIndex);
                      let currAs=currQs[qIndex].answers;
                      let aIndex=currAs.findIndex(currA=>currA.answerID===answer.answerID);
                      if(aIndex!==-1){
                        currAs[aIndex]=answer;
                      }
                      else{
                        currAs.push(answer);
                      }
                      currQs[qIndex].answers=currAs;
                    }
                    catch(err){
                      //console.log(err);
                      //console.log(answer);
                      alert("DODGY ANSWER CAPTURED, SEE CONSOLE! "+moment().format()+"\n"+JSON.stringify(answer));

                      return {};
                    }
                    
                }
              }
            }
            catch(err){
              console.log(err);
            }
            return {questions:currQs};
          });
        });
        
        this.getQuestions();
        this.updateLeaderboard();
      });
      
    }
    
  }

  setChatChannel(channel){
    
    this.setState({chatChannel:channel},()=>{
      try{
        //console.log(this.chatSendRef.current.input.input);
        this.chatSendRef.current.input.focus();
      }
      catch(err){
        //console.log(err)
      }
    });
    
    
  }


  async sendChat(message){
    let response = await fetch("../api/chat",{
      method:"POST",
      credentials:"same-origin",
      headers: {
        'Content-Type': 'application/json'
      },
      body:JSON.stringify({
        channel:this.state.chatChannel,
        userID:this.state.userID,
        userKey:this.state.userKey,
        userName:this.state.userName,
        message:message,
        timestamp:moment().format()
      })
    });
    if(response.ok){
      this.chatSendRef.current.input.setValue("");
      this.setState({chatChannel:0});
    }
    else{
      message.error("Error sending chat message, check your connection & try again");
    }
  }

  async updateLeaderboard(){
    let response = await fetch("../api/leaderboard",{
      method:"GET",
      credentials:"same-origin",
      headers: {
        'Content-Type': 'application/json'
      }
    });
    if(response.ok){
      message.success("Got leaderboard data");
      let data=await response.json();
      //console.log(data);
      this.setState({leaderBoard:data});
      
    }
    else{
      message.error("Unable to get leaderboard, are you logged in?");
    }
  }

  async forceCorrect(answerID){
    
    let answerMarkingCorrect = this.state.questions.find(q=>q.questionID===this.state.currentQuestion).answers.find(a=>a.answerID===answerID).teamAnswer;
    let answerIDsToMarkCorrect = this.state.questions.find(q=>q.questionID===this.state.currentQuestion).answers.filter(a=>a.teamAnswer.toLowerCase()===answerMarkingCorrect.toLowerCase()).filter(a=>a.correct!==true).map(a=>a.answerID);

    for (let answerIDEntry of answerIDsToMarkCorrect){

      let response = await fetch("../api/force?answerID="+answerIDEntry,{
        method:"GET",
        credentials:"same-origin",
        headers: {
          'Content-Type': 'application/json'
        }
      });
      if(response.ok){
        message.success("Forced answerID "+answerIDEntry+" to correct status");
        this.setState(currState=>{
          let currQuestions=currState.questions;
          // //console.log(currQuestions);
          // //console.log(currState.currentQuestion);
          let qIndex=currQuestions.findIndex(q=>q.questionID===currState.currentQuestion);
          // //console.log(qIndex);
          // //console.log(currQuestions[qIndex]);
          let currAnswers=currQuestions[qIndex].answers;
          // //console.log(currAnswers);
          let aIndex = currAnswers.findIndex(a=>a.answerID===answerIDEntry);
          // let currAnswer=currAnswers[aIndex];
          // currAnswer.correct=1;
          // currAnswers[aIndex]=currAnswer;
          currQuestions[qIndex].answers[aIndex].correct=true;
          // //console.log(currQuestions);
          return {questions:currQuestions};
        })
        // this.setState(currState=>{
        //   let currQuestions=currState.questions;
        //   let qIndex=currQuestions.findIndex(q=>q.questionID=questionID);
        //   currQuestions[qIndex].sent=1;
        //   return {questions:currQuestions};
        // });
      }
      else{
        message.error("Error forcing answer "+answerIDEntry+" to correct, are you logged in?");
      }

    }
  }



  async remarkQuestion(questionID){
    
    
    // for (let answerIDEntry of answerIDsToMarkCorrect){

      let response = await fetch("../api/remark?questionID="+questionID,{
        method:"GET",
        credentials:"same-origin",
        headers: {
          'Content-Type': 'application/json'
        }
      });
      if(response.ok){
        message.success("Queued answers for Q "+questionID+" to be re-marked");
        
        // this.setState(currState=>{
        //   let currQuestions=currState.questions;
        //   let qIndex=currQuestions.findIndex(q=>q.questionID=questionID);
        //   currQuestions[qIndex].sent=1;
        //   return {questions:currQuestions};
        // });
      }
      else{
        message.error("Error re-marking Q "+questionID+", are you logged in?");
      }

    // }
  }
  

  async askQuestion(questionID){
    //console.log(questionID);
    let response = await fetch("../api/send?questionID="+questionID,{
      method:"GET",
      credentials:"same-origin",
      headers: {
        'Content-Type': 'application/json'
      }
    });
    if(response.ok){
      message.success("Question "+questionID+" sent to the teams!");
      this.setState(currState=>{
        let currQuestions=currState.questions;
        let qIndex=currQuestions.findIndex(q=>q.questionID===questionID);
        //console.log(qIndex);
        currQuestions[qIndex].sent=1;
        return {questions:currQuestions,currentQuestion:questionID,activeTabRight:"2"};
      });
    }
    else{
      message.error("Error sending quiz question to teams, are you logged in?");
    }
  }


  async viewQuestion(questionID){
    // //console.log(questionID);
    // let response = await fetch("../api/send?questionID="+questionID,{
    //   method:"GET",
    //   credentials:"same-origin",
    //   headers: {
    //     'Content-Type': 'application/json'
    //   }
    // });
    // if(response.ok){
    //   message.success("Question "+questionID+" sent to the teams!");
      this.setState(
        {currentQuestion:questionID,activeTabRight:"2"}
      );
    // }
    // else{
    //   message.error("Error sending quiz question to teams, are you logged in?");
    // }
  }

  async answerQuestion(questionID){
    let response = await fetch("../api/send?a=1&questionID="+questionID,{
      method:"GET",
      credentials:"same-origin",
      headers: {
        'Content-Type': 'application/json'
      }
    });
    if(response.ok){
      message.success("Question "+questionID+" sent to the teams!");
      this.setState(currState=>{
        let currQuestions=currState.questions;
        let qIndex=currQuestions.findIndex(q=>q.questionID===questionID);
        currQuestions[qIndex].closed=1;
        return {questions:currQuestions,currentQuestion:questionID,activeTabRight:"2"};
      });
      
    }
    else{
      message.error("Error sending quiz answer to teams, are you logged in?");
    }
  }

  async getQuestions(quizNum){
    let response = await fetch("../api/getquiz",{
      method:"GET",
      credentials:"same-origin",
      headers: {
        'Content-Type': 'application/json'
      }
    });
    if(response.ok){
      let body = await response.json();
      //console.log(body);
      for(let question of body){
        question.answers=[];
        question.suggestions=[];
      }

      this.setState({questions:body});
    }
    else{
      message.error("Error getting quiz questions, are you logged in?");
    }
  }

  async sendVidLink(link){
    let response = await fetch("../api/setvidlink",{
      method:"POST",
      credentials:"same-origin",
      headers: {
        'Content-Type': 'application/json'
      },
      body:JSON.stringify({
        vidLink:link
      })
    });
    if(response.ok){
      message.success("Video link sent out!");
    }
    else{
      message.error("Error sending video link, check your connection & try again");
    }
  }

  async sendLeaderboard(){
    let response = await fetch("../api/sendleaderboard",{
      method:"GET",
      credentials:"same-origin",
      headers: {
        'Content-Type': 'application/json'
      }
    });
    if(response.ok){
      message.success("leaderboard sent out!");
    }
    else{
      message.error("Error sending leaderboard, check your connection & try again");
    }
  }


  async resetLeaderboard(){
    let response = await fetch("../api/clearleaderboard",{
      method:"POST",
      credentials:"same-origin",
      headers: {
        'Content-Type': 'application/json'
      }
    });
    if(response.ok){
      message.success("LEADERBOARD RESET - SCORES CLEARED");
    }
    else{
      message.error("Error clearing leaderboard, check your connection & try again");
    }
  }

  async changeCaptain(){
    let response = await fetch("../api/changecaptain",{
      method:"POST",
      credentials:"same-origin",
      headers: {
        'Content-Type': 'application/json'
      },
      body:JSON.stringify({
        teamID:this.state.chatChannel
      })
    });
    if(response.ok){
      message.success("Team leader changed for team "+this.state.chatChannel);
    }
    else{
      message.error("Team leader change FAILED for team "+this.state.chatChannel);
    }
  }


  changePanelRight(key){
    this.setState({activeTabRight:key});
  }

  async addQuestion(round){
    //console.log("Adding question to round "+round);





    let nextQuestionNumber = Math.max(...this.state.questions.filter(q=>q.roundNumber===round).map(q=>q.questionNumber))+1;
    nextQuestionNumber = nextQuestionNumber>0 ? nextQuestionNumber : 1
    //console.log(nextQuestionNumber);
    let response = await fetch("../api/newQuestion",{
      method:"POST",
      credentials:"same-origin",
      headers: {
        'Content-Type': 'application/json'
      },
      body:JSON.stringify({
        roundNumber:round,
        questionNumber:nextQuestionNumber
      })
    });
    
    if(response.ok){
      let data=await response.json();
      let newQuestions = [...this.state.questions];
      newQuestions.push({roundNumber:round,questionNumber:nextQuestionNumber,questionID:data.questionID,question:"",displayAnswer:"",answer:'{"contains":""}',pointValue:1});
      this.setState({questions:newQuestions});
    }
    else{
      message.error("Failed to add...");
    }
  }


  async addRound(){
    let nextRound = Math.max(...this.state.questions.map(question=>question.roundNumber))+1;
    nextRound = nextRound>0 ? nextRound : 1;
    //console.log("Attempting to create new round numbered "+nextRound)
    await this.addQuestion(nextRound);
  }


  async newQuiz(){
    let response = await fetch("../api/newQuiz",{
      method:"POST",
      credentials:"same-origin",
      headers: {
        'Content-Type': 'application/json'
      }
    });
    
    if(response.ok){
      window.location.reload();
    }
    else{
      message.error("Failed to add...");
    }
  }

  render(){
    


    return (
      <Tabs defaultActiveKey={1}>
        <Tabs.TabPane tab={"Hosting Interface"} key={1}>
        {/* {JSON.stringify(this.state)} */}
        <Row gutter={15}>
          <Col span={6}>
          <Input.Search ref={this.vidSendRef} enterButton={"Set video link"} onSearch={value => this.sendVidLink(value)}/><hr/>
            
            <div className="chat-area" ref={this.chatBoxRef}>
              <Tabs defaultActiveKey={1}>
                <Tabs.TabPane tab={"All Chat"} key={1}>
              {this.state.chatMessages.length>0?
            
                <Table 
                  // title={()=>{return <Title level={2}>Chat Panel</Title>}}
                  dataSource={this.state.chatMessages.slice(this.state.chatMessages.length-50)} 
                  columns={[
                    {title:"Team",dataIndex:"teamName",key:"teamName"},
                    {title:"User",dataIndex:"user",key:"user",
                      render:(a,record)=>(<Typography.Text style={{backgroundColor:a.includes("(Host)")||a.includes("(Admin)")?"#FF0000":(a.includes("@Wife")?"#FF1493":null)}}>{a}</Typography.Text>)
                    },
                    {title:"Message",dataIndex:"text",key:"text",
                      // render:(a,record)=>(<Typography.Text onClick={()=>this.setChatChannel(record.channel)} style={{backgroundColor:record.text.toLowerCase().includes("@host")?"#FF0000":(record.text.toLowerCase().includes("@wife")?"#FF1493":null)}}>{record.text}</Typography.Text>)
                      render:(a,record)=>(<Typography.Text style={{backgroundColor:record.text.toLowerCase().includes("@host")?"#FF0000":(record.text.toLowerCase().includes("@wife")?"#FF1493":null)}}>{record.text}</Typography.Text>)
                    }
                  ]}
                  // style={{height:700}}
                  scroll={{y:660}} 
                  pagination={false}
                  
                  onRow={(record,rowIndex)=>{
                    return {onClick:(e)=>{e.preventDefault();this.setChatChannel(record.channel)}}
                  }}
                />
                
              :
                null
              }
</Tabs.TabPane>
<Tabs.TabPane tab={"Focussed Chat"} key={2}>

              {this.state.specialMessages.length>0?
            
                <Table 
                  // title={()=>{return <Title level={2}>Chat Panel</Title>}}
                  dataSource={this.state.specialMessages} 
                  columns={[
                    {title:"Team",dataIndex:"teamName",key:"teamName"},
                    {title:"User",dataIndex:"user",key:"user",
                      render:(a,record)=>(<Typography.Text style={{backgroundColor:a.includes("(Host)")||a.includes("(Admin)")?"#FF0000":(a.includes("@Wife")?"#FF1493":null)}}>{a}</Typography.Text>)
                    },
                    {title:"Message",dataIndex:"text",key:"text",
                      // render:(a,record)=>(<Typography.Text onClick={()=>this.setChatChannel(record.channel)} style={{backgroundColor:record.text.toLowerCase().includes("@host")?"#FF0000":(record.text.toLowerCase().includes("@wife")?"#FF1493":null)}}>{record.text}</Typography.Text>)
                      render:(a,record)=>(<Typography.Text  style={{backgroundColor:record.text.toLowerCase().includes("@host")?"#FF0000":(record.text.toLowerCase().includes("@wife")?"#FF1493":null)}}>{record.text}</Typography.Text>)
                    }
                  ]}
                  // style={{height:700}}
                  scroll={{y:660}} 
                  pagination={false}
                  onRow={(record,rowIndex)=>{
                    return {onClick:(e)=>{e.preventDefault();this.setChatChannel(record.channel)}}
                  }}
                />
                
              :
                null
              }
</Tabs.TabPane>

              </Tabs>



              {/* <Form> */}
                {/* <Affix offsetBottom={0}> */}
                  {/* <Form.Item> */}
                    <Input.Search ref={this.chatSendRef} disabled={this.state.chatChannel===0}  enterButton={"Send to channel "+this.state.chatChannel} size="large" onSearch={value => this.sendChat(value)}/>
                    <Popconfirm onConfirm={this.changeCaptain} title={"Force change of captain for team "+this.state.chatChannel+"?"} okText={"Yes, Proceed"} cancelText={"Nope, didn't mean to click that!"}><Button disabled={this.state.chatChannel===0} type={"danger"}>Force Change Of Captaincy For Team {this.state.chatChannel}</Button></Popconfirm>

                  {/* </Form.Item> */}
                {/* </Affix> */}
              {/* </Form> */}
              
              
              
              
            </div>
          </Col>

          <Col span={12}>
            {this.state.questions.length>0?
            <>
              <Table 
                title={()=>{return <Title level={2}>Question Controls</Title>}}
                size="small"
                dataSource={this.state.questions} 
                columns={[
                  {title:"R-Q",dataIndex:"roundNumber",key:"questionID",width:50,render:(r,question)=>{return question.roundNumber + "-" + question.questionNumber}},
                  
                  {title:"Send Question",dataIndex:"questionID",key:"questionID",render:(a,question)=>(<><Button onClick={()=>{this.askQuestion(question.questionID)}} disabled={question.closed===1} style={{backgroundColor:question.sent===1?"#00AA00":null}}>Send Question</Button><br/><Button onClick={()=>{this.viewQuestion(question.questionID)}} type="primary">View Question</Button></>)},
                  
                  {title:"Question",dataIndex:"question",key:"questionID"},
                  {title:"Answer",dataIndex:"displayAnswer",key:"questionID"},
                  {title:"Answer Question",dataIndex:"questionID",key:"questionID",render:(a,question)=>(<><Button onClick={()=>{this.answerQuestion(question.questionID)}} style={{backgroundColor:question.closed===1?"#00AA00":null}}>Send Answer</Button><br/><Button onClick={()=>{this.remarkQuestion(question.questionID)}} style={{backgroundColor:question.closed===1?"#00AA00":null}}>Re-Mark Answers</Button></>)},
                ]}
                pagination={10}
                scroll={{y:700}}
              />
              {/* {JSON.stringify(this.state.questions)} */}
              </>
              // null
            :
            <>No questions your honor...</>}
          </Col>

          <Col span={6}>


          <Tabs activeKey={this.state.activeTabRight} onChange={this.changePanelRight}>
                <Tabs.TabPane tab={"Leaderboard"} key={"1"}>

          {this.state.leaderBoard.length>0?
              // <>{this.state.leaderBoard.length}</>
              
              <Table 
                // title={()=>{return <>Leaderboard<div style={{position:"absolute",right:0,top:0}}><Button onClick={()=>{this.updateLeaderboard()}}>Refresh</Button><Button type="primary" onClick={()=>{this.sendLeaderboard()}}>Publish</Button><Popconfirm onConfirm={()=>{this.resetLeaderboard()}}><Button>DELETE SCORES</Button></Popconfirm></div></>}}
                title={()=>{return <>Leaderboard<div style={{position:"absolute",right:0,top:0}}><Button onClick={()=>{this.updateLeaderboard()}}>Refresh</Button><Button type="primary" onClick={()=>{this.sendLeaderboard()}}>Publish</Button></div></>}}
              //   title={()=>"header"}
              //   // title={()=>{return <>Leaderboard<Button style={{float:"right"}} onClick={()=>{this.updateLeaderboard()}}>Refresh</Button></>}} //
                dataSource={this.state.leaderBoard}
                size="small"
                columns={[
                  {title:"Position",dataIndex:"teamID",key:"teamID",render:(t,team,index)=><>{index}</>},
                  {title:"Team",dataIndex:"teamID",key:"teamID",render:(t,team)=><>{team.teamName}#{team.teamID}</>},
                  {title:"Score",dataIndex:"score",key:"teamID"}
                ]}
                // item=><List.Item>{item.teamName}#{item.teamID} - {item.score} points</List.Item>
                pagination={false}
                scroll={{y:700}}
              />
              // JSON.stringify(this.state.leaderBoard)
            :
            <>No answers yet your honor... <Button style={{float:"right"}} onClick={()=>{this.updateLeaderboard()}}>Refresh</Button></>}

            </Tabs.TabPane>

            <Tabs.TabPane tab={"Team Answers"} key={"2"}>
            {this.state.questions.findIndex(q=>q.questionID===this.state.currentQuestion)!==-1?
              <>


                {this.state.questions.find(q=>q.questionID===this.state.currentQuestion).answers.length>0?


                  <Table 
                    title={()=>{return <Title level={2}>Team Answers</Title>}}
                    size="small"
                    dataSource={this.state.questions.find(q=>q.questionID===this.state.currentQuestion).answers.sort((a,b)=>{return (a.correct === b.correct) ? (a.teamAnswer.toLowerCase()>b.teamAnswer.toLowerCase()?1:-1) : b.correct ? -1 : 1;})}

                    columns={[
                      
                      {title:"Team",dataIndex:"team",key:"team",render:(a,answer)=>{
                        let returnValue;
                        try{
                          returnValue = answer.team+"#"+answer.teamID;
                        }
                        catch(err){
                          //console.log("ERR TEAM ANSWER")
                          console.error(answer);
                          returnValue="ERR";
                        }
                        return returnValue;

                      }},
                      {title:"Answer",dataIndex:"teamAnswer",key:"answerID",render:(a,answer)=>{
                        let returnValue;
                        try{
                          returnValue = <Typography style={{backgroundColor:answer.correct===true?"#00FF00":"#FF0000"}}>{answer.teamAnswer}</Typography>;
                        }
                        catch(err){
                          //console.log("ERR TEAM ANSWER")
                          console.error(answer);
                          returnValue="ERR";
                        }
                        return returnValue;
                      }},
                      {title:"Force Correct",dataIndex:"questionID",key:"answerID",render:(a,answer)=>{
                        let returnValue;
                        try{
                          returnValue = <Button onClick={answer.answerID?()=>{this.forceCorrect(answer.answerID)}:null} disabled={answer.correct===true}>Mark Correct</Button>
                        }
                        catch(err){
                          //console.log("ERR TEAM ANSWER")
                          console.error(answer);
                          returnValue="ERR";
                        }
                        return returnValue;
                      }}
                    ]}
                    pagination={false}
                    scroll={{y:700}}
                  />

                
                :
                <>No answers yet your honor...</>}

                {/* {this.state.questions.find(q=>q.questionID===this.state.currentQuestion).suggestions.length>0?
                  <Table 
                    title={()=>{"Team Suggestions"}}
                    size="small"
                    dataSource={this.state.questions.find(q=>q.questionID===this.state.currentQuestion).suggestions.filter(suggestion=>suggestion.selected!==true)} 
                    columns={[
                      
                      {title:"Team",dataIndex:"team",key:"team",render:(a,answer)=>answer.team+"#"+answer.teamID},
                      {title:"Player",dataIndex:"user",key:"user"},
                      {title:"Suggestion",dataIndex:"answer",key:"answer"}
                    ]}
                    pagination={false}
                    scroll={{y:300}}
                  />
                : <>No Suggestions for question yet...</>} */}
              
              
              </>
              :
              <>Ask a question then?</>
            }
</Tabs.TabPane>
            </Tabs>
            
          </Col>

        </Row>
        </Tabs.TabPane>
        <Tabs.TabPane tab={"Quiz Setup"} key={2}>
          <Collapse accordion>
            {this.state.questions.map(question=>question.roundNumber).filter((r,i,a)=>a.indexOf(r)===i).map(round=>{
              return (
              <Collapse.Panel header={"Round " + round} key={round}>
                <EditableFormTable 
                  
                  // footer={<Button>Add question to round {round}</Button>}
                  stateSetter={this.stateSetter}
                  bordered
                  data={this.state.questions.filter(q=>q.roundNumber===round)}
                  allData={this.state.questions}
                  // renderItem={question => <List.Item>{question.questionNumber}</List.Item>}
                />
                <Button onClick={()=>{this.addQuestion(round)}}>Add Question To Round</Button>
              </Collapse.Panel>
              )
            })}
            
          </Collapse>
          <Button onClick={this.addRound}>Add Round</Button>
          <Popconfirm onConfirm={this.newQuiz} title="This will archive all questions and the current leaderboard then create a new, empty, quiz - are you sure you want to proceed?"><Button>Create a new quiz</Button></Popconfirm>
        </Tabs.TabPane>
      </Tabs>
    );
  }
  
}




const EditableContext = React.createContext();

class EditableCell extends React.Component {
  getInput = (record) => {
    if (this.props.inputType === 'number') {
      return <InputNumber />;
    }
    else if(this.props.inputType==="image"){
      return (<Upload
        withCredentials
        onChange={(info)=>{
          if (info.file.status !== 'uploading') {
            //console.log(info.file, info.fileList);
          }
          if (info.file.status === 'done') {
            message.success(`${info.file.name} file uploaded successfully`);
            // //console.log(info);
          } else if (info.file.status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
          }
        }}
        action={"../api/upload"}
        data={{questionID:record.questionID}}
      >
        <Button icon="upload">
          Click to Upload
        </Button>
      </Upload>);
    }
    else{
      return <Input />;
    }
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              rules: [
                {
                  required: dataIndex==="img"?false:true,
                  message: `Please Input ${title}!`,
                },
              ],
              initialValue: record[dataIndex],
            })(this.getInput(record))}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  render() {
    return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>;
  }
}

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = { editingKey: '' };
    this.columns = [
      {
        title: 'Question #',
        dataIndex: 'questionNumber',
        width: '40',
        align:"center",
        editable: false,
        dataType:"number"
      },
      {
        title: 'Question ID',
        dataIndex: 'questionID',
        width: '40',
        align:"center",
        editable: false,
        dataType:"number"
      },
      {
        title: 'Question',
        dataIndex: 'question',
        // width: '15%',
        editable: true,
        dataType:"text"
      },
      {
        title: 'Display Answer',
        dataIndex: 'displayAnswer',
        // width: '40%',
        editable: true,
        dataType:"text"
      },
      {
        title: 'Auto Marker',
        dataIndex: 'answer',
        // width: '40%',
        editable: true,
        dataType:"text"
      },
      {
        title: 'Image',
        dataIndex: 'img',
        // width: '40%',
        render:(img)=>{return img!==undefined?<img src={"../"+img}/>:null},
        editable: true,
        dataType:"image"
      },
      {
        title: 'Points',
        dataIndex: 'pointValue',
        width: '40',
        // render:(img)=><img src={"../"+img}/>,
        editable: true,
        dataType:"number"
      },
      {
        title: 'operation',
        dataIndex: 'operation',
        render: (text, record) => {
          const { editingKey } = this.state;
          const editable = this.isEditing(record);
          return editable ? (
            <span>
              <EditableContext.Consumer>
                {form => (
                  <a
                    onClick={() => this.save(form, record.questionID)}
                    style={{ marginRight: 8 }}
                  >
                    Save
                  </a>
                )}
              </EditableContext.Consumer>
              <Popconfirm title="Discard changes?" onConfirm={() => this.cancel(record.questionID)}>
                <a>Cancel</a>
              </Popconfirm>
            </span>
          ) : (
            <a disabled={editingKey !== ''} onClick={() => this.edit(record.questionID)}>
              Edit
            </a>
          );
        },
      },
    ];
  }

  isEditing = record => record.questionID === this.state.editingKey;

  cancel = () => {
    this.setState({ editingKey: '' });
  };

  save(form, key) {
    form.validateFields(async (error, row) => {
      if (error) {
        return;
      }
      //console.log(key);
      //console.log(row);
      let response = await fetch("../api/set",{
        method:"POST",
        credentials:"same-origin",
        headers: {
          'Content-Type': 'application/json'
        },
        body:JSON.stringify({
          questionID:key,
          ...row
        })
      });
      if(response.ok){
        message.success("Question updated!");
        // window.location.reload();
      }
      else{
        message.error("Failed...");
      }
      const newData = [...this.props.allData];
      const index = newData.findIndex(item => key === item.questionID);
      if (index > -1) {
        //console.log("found, updating");
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        
      } else {
        //console.log("not found, pushing (probably wrong?)")
        newData.push(row);
        
      }
      this.props.stateSetter({ questions: newData});
      this.setState({ editingKey: '' });
    });
  }

  edit(key) {
    this.setState({ editingKey: key });
  }

  render() {
    const components = {
      body: {
        cell: EditableCell,
      },
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.dataType,
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record),
        }),
      };
    });

    return (
      <EditableContext.Provider value={this.props.form}>
        <Table
          components={components}
          bordered
          dataSource={this.props.data}
          columns={columns}
          rowClassName="editable-row"
          pagination={false}
        />
      </EditableContext.Provider>
    );
  }
}

const EditableFormTable = Form.create()(EditableTable);




export default App;

