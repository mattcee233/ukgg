
class AnswerEngine {
    constructor(db, faye, serverKey) {
        this.db = db;
        this.faye = faye;
        this.serverKey = serverKey;
    }

    async checkAnswers() {
        const start = process.hrtime();
        let answerData = {};
        let correctArray = [];
        let incorrectArray = [];
        const [answerRecords] = await this.db.query("SELECT a.teamAnswer, q.answer AS actualAnswer, q.displayAnswer, q.answerType, teamName, a.teamID, a.questionID, answerID, a.quizID FROM answers a LEFT JOIN questions q ON a.questionID = q.questionID LEFT JOIN teams t ON a.teamID = t.teamID WHERE marked = 0;");
        //console.log(`found ${answerRecords.length} answers to check`);

        for (const record of answerRecords) {
            let actualAnswer;
            try {
                actualAnswer = JSON.parse(record.actualAnswer);
            } catch (e) {
                //console.log(`failed to parse actualAnswer for question ID ${record.questionID}:`);
                //console.log(e);
                continue;
            }

            let isCorrect;
            try {
                isCorrect = await this.testAnswer(record.teamAnswer, actualAnswer, Object.keys(actualAnswer).sort()[0]);
            } catch (e) {
                //console.log(`failed to test answer ID ${record.answerID}:`);
                //console.log(e);
                continue;
            }

            
            if(isCorrect){
                // await this.db.query("UPDATE answers SET correct=1, marked=1 WHERE answerID=?;", record.answerID);
                correctArray.push(record.answerID);
            }
            else{
                // await this.db.query("UPDATE answers SET correct=0, marked=1 WHERE answerID=?;", record.answerID);
                incorrectArray.push(record.answerID);
            }

            if(typeof answerData[record.quizID]==="undefined"){
                answerData[record.quizID]=[];
            }

            answerData[record.quizID].push({
                quizID:record.quizID,
                questionID: record.questionID,
                teamAnswer: record.teamAnswer,
                actualAnswer: record.displayAnswer,
                answerID: record.answerID,
                team: record.teamName,
                teamID: record.teamID,
                correct: isCorrect,     
            });
        }

        //console.log(answerData);

        for(let quizID of Object.keys(answerData)){
            //console.log(answerData[quizID]);
            this.faye.publish("/"+quizID+"/host/answers", {data:answerData[quizID],ext: {serverKey: this.serverKey}});
        }

        
        console.log(correctArray);
        if(correctArray.length>0){
            await this.db.query("UPDATE answers SET correct=1, marked=1 WHERE answerID IN (?);", [correctArray]);
        }

        console.log(incorrectArray);
        if(incorrectArray.length>0){
            await this.db.query("UPDATE answers SET correct=0, marked=1 WHERE answerID IN (?);", [incorrectArray]);
        }
        

        const timeDiff = process.hrtime(start);
        const timeStr = timeDiff[0] + " s, " + (timeDiff[1] / 1000000).toFixed(3) + " ms ";
        //console.log(`completed ${answerRecords.length}, and took: ${timeStr}`);

        setTimeout(this.checkAnswers.bind(this), 10000);
    }

    async testAnswer(teamAnswer, actualAnswer, answerType) {
        teamAnswer = teamAnswer.toString().toLowerCase();

        switch (answerType) {
            case "equals":
                return (teamAnswer === actualAnswer.equals.toString().toLowerCase());
            case "max":
                teamAnswer = parseFloat(teamAnswer.replace(/[^\d.]/g,""));
                if (isNaN(teamAnswer)) {
                    return false;
                }

                return (teamAnswer >= actualAnswer.min && teamAnswer <= actualAnswer.max);
            case "contains":
                return actualAnswer.contains.split("||").some(orAnswers=>{
                    return orAnswers.split("&&").every(andAnswer=>{
                        return teamAnswer.includes(andAnswer.toString().toLowerCase());
                    });
                });
            default:
                return false;
        }
    };
}

module.exports = {
    AnswerEngine
};

if (require.main === module) {
    const envVars = require('dotenv').config();
    const faye = require("faye");
    const mysql = require('mysql2');

    if (!process.env.serverHost || !process.env.serverPort) {
        throw new Error("Missing required environment variables 'serverHost' and 'serverPort'");
    }

    //console.log(`Starting answer engine`);

    const serverKey=process.env.serverKey;
    const fayeClient = new faye.Client(process.env.serverHost+":"+process.env.serverPort+"/websocket");
    fayeClient.connect();
    //console.log(`connected to: ${process.env.serverHost}:${process.env.serverPort}/websocket`);

    const dbConfig = {
        host:process.env.dbHost,
        // port:process.env.dbPort,
        user:process.env.dbUser,
        password:process.env.dbPass,
        database:process.env.dbSchema,
        connectionLimit:10
    };
    const dbConnection = mysql.createPool(dbConfig).promise();

    //console.log(`Answer engine finished startup`);
    const answerEngine = new AnswerEngine(dbConnection, fayeClient, serverKey);
    answerEngine.checkAnswers().then(() => {
        //console.log(`initial check complete`);
    });
}
